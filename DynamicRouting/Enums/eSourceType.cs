﻿namespace DynamicRouting
{
    public enum eSourceType : int
    {
        Blank,
        Pc,
        Vtc,
        Camera,
        Aux,
        Microphone
    }
}
