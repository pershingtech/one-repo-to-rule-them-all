﻿namespace DynamicRouting
{
    public enum eDestinationType : int
    {
        Display,
        Audio,
        VtcCamera,
        VtcContent
    }
}
