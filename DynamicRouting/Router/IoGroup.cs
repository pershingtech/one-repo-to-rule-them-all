﻿using Newtonsoft.Json;

namespace DynamicRouting
{
    public class IoGroup
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("designator")]
        public ushort Designator { get; set; }
    }
}
