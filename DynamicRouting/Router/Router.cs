﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Net.Http;
using System.Linq;
using Newtonsoft.Json.Linq;

namespace DynamicRouting
{
    public static class Router
    {
        //fields
        public static List<IoDevice> IoDevices = new List<IoDevice>();
        public static List<IoGroup> SourceGroups = new List<IoGroup>();
        public static List<IoGroup> DestinationGroups = new List<IoGroup>();
        public static List<Classification> Classifications = new List<Classification>();

        private static IRouterImplementation RouterImplementation;
        
        //constructor
        static Router()
        {
            //subscribe to unsolicited route change events from the router implementation
            //RouterImplementation.RouteChanged += RouterImplementation_RouteChanged;
        }

        /***This section handles events raised by the RouterImplementation***/
        public static event EventHandler<RouteEventArgs> RouteChanged;
        private static void RouterImplementation_RouteChanged(object sender, RouteEventArgs e)
        {
            //called from unsoliciated route changes by RouterImplementation
            //raise the event that the route changed (listened to by RoutingService to set feedback text)
            RouteChanged?.BeginInvoke(null, new RouteEventArgs(e.Source, e.Destination, e.RouteType), EndRouteChanged, null);
        }
        private static void EndRouteChanged(IAsyncResult Result)
        {
            var ar = (System.Runtime.Remoting.Messaging.AsyncResult)Result;
            var invokedMethod = (EventHandler)ar.AsyncDelegate;
            try
            {
                invokedMethod.EndInvoke(Result);
            }
            catch
            {
                Console.WriteLine("A route event handler broke");
            }
        }

        public static event EventHandler ConfigurationChanged;

        public static async Task ConfigureFromWebService(string Url)
        {
            try
            {
                //asynchronously GET JSON from Url
                HttpClient httpClient = new HttpClient();
                HttpResponseMessage response = await httpClient.GetAsync(Url);
                string content = await response.Content.ReadAsStringAsync();
                ProcessJson(content);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
        private static void ProcessJson(string JString)
        {
            try
            {
                JObject jConfig = JObject.Parse(JString);

                var jIoDevices = jConfig.SelectToken("ioDevices");
                if (jIoDevices != null) IoDevices = jIoDevices.ToObject<List<IoDevice>>();
                IoDevices = IoDevices.OrderBy(x => x.UiName).ToList();

                var jSourceGroups = jConfig.SelectToken("ioGroups.sourceGroups");
                if (jSourceGroups != null) SourceGroups = jSourceGroups.ToObject<List<IoGroup>>();
                SourceGroups = SourceGroups.OrderBy(x => x.Designator).ToList();

                var jDestGroups = jConfig.SelectToken("ioGroups.destinationGroups");
                if (jDestGroups != null) DestinationGroups = jDestGroups.ToObject<List<IoGroup>>();
                DestinationGroups = DestinationGroups.OrderBy(x => x.Designator).ToList();

                var jClassifications = jConfig.SelectToken("classifications");
                if (jClassifications != null) Classifications = jClassifications.ToObject<List<Classification>>();
                Classifications = Classifications.OrderBy(x => x.Id).ToList();

                ConfigurationChanged?.Invoke(null, new EventArgs());
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}