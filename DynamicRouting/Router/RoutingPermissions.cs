﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DynamicRouting
{
    public static class RoutingPermissions
    {
        //routing permissions
        public static bool UseClassificationCheck { get; set; }
        public static bool UseTypeCheck { get; set; }

        public static Dictionary<int, List<int>> Classifications = new Dictionary<int, List<int>>();

        public static bool ClassificationOk(int SourceClass, int DestinationClass)
        {
            if(UseClassificationCheck)
            {
                if(Classifications.ContainsKey(SourceClass))
                {
                    return Classifications[SourceClass].Contains(DestinationClass);
                }
                throw new Exception($"Source classification {SourceClass} undefined");
            }
            return true;
        }

        public static bool TypeOk(eSourceType SourceType, eDestinationType DestinationType)
        {
            if (UseTypeCheck)
            {
                switch (SourceType)
                {
                    case eSourceType.Blank: { return true; }
                    case eSourceType.Pc:
                    case eSourceType.Aux:
                        { return (DestinationType != eDestinationType.VtcCamera); } //PC can route anywhere but VTC camera
                    case eSourceType.Vtc: { return (DestinationType != eDestinationType.VtcCamera && DestinationType != eDestinationType.VtcContent); }
                    case eSourceType.Camera: { return (DestinationType != eDestinationType.VtcContent); }
                    case eSourceType.Microphone: { return (DestinationType != eDestinationType.VtcContent && DestinationType != eDestinationType.Display); }
                }

                throw new Exception($"Source type {SourceType} undefined");
            }
            return true;
        }
    }
}