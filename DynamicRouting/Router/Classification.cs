﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace DynamicRouting
{
    public class Classification
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("canRouteTo")]
        public List<int> CanRouteTo { get; set; }
    }
}
