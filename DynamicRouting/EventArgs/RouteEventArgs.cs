﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DynamicRouting
{
    public class RouteEventArgs : EventArgs
    {
        public RouteEventArgs(Source Source, Destination Destination, eRouteType RouteType)
        {
            this.Source = Source;
            this.Destination = Destination;
            this.RouteType = RouteType;
        }
        public Source Source;
        public Destination Destination;
        public eRouteType RouteType;
    }
}
