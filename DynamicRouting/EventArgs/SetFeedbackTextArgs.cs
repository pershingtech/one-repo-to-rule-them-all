﻿using System;

namespace DynamicRouting
{
    public class SetFeedbackTextArgs : EventArgs
    {
        public int IoDeviceIndex;
        public int OutputIndex;
        public string SourceName;
    }
}
