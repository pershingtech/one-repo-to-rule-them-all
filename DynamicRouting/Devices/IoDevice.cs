﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace DynamicRouting
{
    public class IoDevice
    {
        [JsonProperty("uiName")]
        public string UiName { get; set; }

        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("inputs")]
        public List<Source> Sources = new List<Source>();

        [JsonProperty("outputs")]
        public List<Destination> Destinations = new List<Destination>();

        [JsonProperty("sourceGroups")]
        public List<long> SourceGroups = new List<long>();

        [JsonProperty("destinationGroups")]
        public List<long> DestinationGroups = new List<long>();

        [JsonProperty("touchPanels")]
        public List<long> AssignedTouchPanels = new List<long>();
    }
}
