﻿using Newtonsoft.Json;

namespace DynamicRouting
{
    public class Source
    {
        /*
         * Identifier type depends on the routing platform
         * being used. For example, in NVX this would be a
         * string and would be the hostname. For traditional
         * matrix switcher, this would be an integer for the
         * source's input number
         */
        [JsonProperty("identifier")]
        public object Identifier { get; set; }

        [JsonProperty("uiName")]
        public string FriendlyName { get; set; }

        //TODO: need to figure out how to map between JSON value and enum
        //[JsonProperty("type")]
        public eSourceType SourceType { get; set; }

        [JsonProperty("classification")]
        public int Classification { get; set; }

        [JsonProperty("id")]
        public long UniqueId { get; set; }
    }
}