﻿using System;
using Newtonsoft.Json;

namespace DynamicRouting
{
    public class Destination
    {
        //event is called to the static Router when a route should be made
        public event EventHandler<RouteEventArgs> MakeRoute;

        /*
         * Identifier type depends on the routing platform
         * being used. For example, in NVX this would be a
         * string and would be the hostname. For traditional
         * matrix switcher, this would be an integer for the
         * destinations's output number
         */
        [JsonProperty("identifier")]
        public object Identifier { get; internal set; }

        [JsonProperty("uiName")]
        public string FriendlyName { get; internal set; }

        //[JsonProperty("type")]
        public eDestinationType DestinationType { get; internal set; }

        [JsonProperty("classification")]
        public int Classification { get; internal set; }

        [JsonProperty("id")]
        public long UniqueId { get; set; }

        public Source CurrentSource { get; private set; }

        public void SetNewSource(Source NewSource)
        {
            if(RoutingPermissions.ClassificationOk(NewSource.Classification, this.Classification) &&
                RoutingPermissions.TypeOk(NewSource.SourceType, this.DestinationType))
            {
                CurrentSource = NewSource;
                
                //TODO: update the eRouteType used in this event based on the routing layers used by this destination & source
                MakeRoute?.BeginInvoke(this, new RouteEventArgs(NewSource, this, eRouteType.Video), EndMakeRoute, null);
            }
        }
        private void EndMakeRoute(IAsyncResult Result)
        {
            var ar = (System.Runtime.Remoting.Messaging.AsyncResult)Result;
            var invokedMethod = (EventHandler)ar.AsyncDelegate;
            try
            {
                invokedMethod.EndInvoke(Result);
            }
            catch (Exception e)
            {
                Console.WriteLine("A route event handler broke");
                Console.WriteLine(e.Message);
            }
        }
    }
}
