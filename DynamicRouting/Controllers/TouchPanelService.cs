﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DynamicRouting
{
    public class TouchPanelService
    {
        private List<IoGroup> MySourceGroups = new List<IoGroup>();
        private List<IoGroup> MyDestinationGroups = new List<IoGroup>();

        public TouchPanelService()
        {
            Router.ConfigurationChanged += Router_ConfigurationChanged;
        }

        public event EventHandler<ConfigIoGroupArgs> SetSourceGroup;
        public event EventHandler<ConfigIoGroupArgs> SetDestinationGroup;
        public event EventHandler<ConfigIoGroupCollectionArgs> SetSourceIoDevice;
        public event EventHandler<ConfigIoGroupCollectionArgs> SetDestinationIoDevice;

        public long Id { get; set; }

        public ushort SelectSourceGroup(ushort GroupIndex)
        {
            if(MySourceGroups.Count > GroupIndex)
            {
                //select all devices that are in the desired group and are assigned to this touch panel
                var selectedDevices = Router.IoDevices
                    .Where(x => x.SourceGroups.Contains(MySourceGroups[GroupIndex].Id))
                    .Where(y => y.AssignedTouchPanels.Contains(Id))
                    .ToList();


                for(int ioDeviceIndex = 0; ioDeviceIndex < selectedDevices.Count; ioDeviceIndex++)
                {
                    for(int sourceIndex = 0; sourceIndex < selectedDevices[ioDeviceIndex].Sources.Count; sourceIndex++)
                    {
                        SetSourceIoDevice?.Invoke(this, new ConfigIoGroupCollectionArgs()
                        {
                            IoDeviceIndex = (ushort)ioDeviceIndex,
                            IoDeviceName = selectedDevices[ioDeviceIndex].UiName,
                            SourceOrDestIndex = (ushort)sourceIndex,
                            SourceOrDestName = selectedDevices[ioDeviceIndex].Sources[sourceIndex].FriendlyName
                        }) ;
                    }
                }

                return (ushort)MySourceGroups.Count;
            }
            else
            {
                Console.WriteLine("Invalid source group index entered");
                return 0;
            }
        }
        public ushort SelectDestinationGroup(ushort GroupIndex)
        {
            if (MyDestinationGroups.Count > GroupIndex)
            {
                //select all devices that are in the desired group and are assigned to this touch panel
                var selectedDevices = Router.IoDevices
                    .Where(x => x.DestinationGroups.Contains(MyDestinationGroups[GroupIndex].Id))
                    .Where(y => y.AssignedTouchPanels.Contains(Id))
                    .ToList();


                for (int ioDeviceIndex = 0; ioDeviceIndex < selectedDevices.Count; ioDeviceIndex++)
                {
                    for (int destIndex = 0; destIndex < selectedDevices[ioDeviceIndex].Destinations.Count; destIndex++)
                    {
                        SetDestinationIoDevice?.Invoke(this, new ConfigIoGroupCollectionArgs()
                        {
                            IoDeviceIndex = (ushort)ioDeviceIndex,
                            IoDeviceName = selectedDevices[ioDeviceIndex].UiName,
                            SourceOrDestIndex = (ushort)destIndex,
                            SourceOrDestName = selectedDevices[ioDeviceIndex].Destinations[destIndex].FriendlyName
                        });
                    }
                }

                return (ushort)MySourceGroups.Count;
            }
            else
            {
                Console.WriteLine("Invalid source group index entered");
                return 0;
            }
        }


        //method sets all the Source and Destination group names on the Touch Panel
        private void Router_ConfigurationChanged(object sender, EventArgs e)
        {
            //get all my devices first
            var myIoDevices = Router.IoDevices.Where(x => x.AssignedTouchPanels.Contains(Id));
            //get a list of all my source group IDs
            var mySourceGroupIDs = myIoDevices.SelectMany(x => x.SourceGroups).Distinct();
            //get the list of my groups (IoGroup classes)
            MySourceGroups = Router.SourceGroups.Where(x => mySourceGroupIDs.Contains(x.Id)).ToList();

            for (int i = 0; i < MySourceGroups.Count; i++)
            {
                SetSourceGroup?.Invoke(this, new ConfigIoGroupArgs()
                {
                    Index = (ushort)i,
                    GroupName = MySourceGroups[i].Name
                }) ;
            }

            //same thing but for destination groups
            var myDestGroupIDs = myIoDevices.SelectMany(x => x.DestinationGroups).Distinct();
            MyDestinationGroups = Router.DestinationGroups.Where(x => myDestGroupIDs.Contains(x.Id)).ToList();
            for (int i = 0; i < MyDestinationGroups.Count; i++)
            {
                SetDestinationGroup?.Invoke(this, new ConfigIoGroupArgs()
                {
                    Index = (ushort)i,
                    GroupName = MyDestinationGroups[i].Name
                });
            }
        }
    }
}