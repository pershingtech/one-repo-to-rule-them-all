﻿using System;

namespace DynamicRouting
{
    public class ConfigIoGroupCollectionArgs : EventArgs
    {
        public ushort IoDeviceIndex { get; set; }
        public string IoDeviceName { get; set; }
        public ushort SourceOrDestIndex { get; set; }
        public string SourceOrDestName { get; set; }
    }
}
