﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DynamicRouting
{
    public class ConfigIoGroupArgs : EventArgs
    {
        public ushort Index { get; set; }
        public string GroupName { get; set; }
    }
}
