using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Linq;
using Crestron;
using Crestron.Logos.SplusLibrary;
using Crestron.Logos.SplusObjects;
using Crestron.SimplSharp;

namespace UserModule_TOUCHPANELSERVICE
{
    public class UserModuleClass_TOUCHPANELSERVICE : SplusObject
    {
        static CCriticalSection g_criticalSection = new CCriticalSection();
        
        
        
        
        
        
        
        UShortParameter ID;
        InOutArray<Crestron.Logos.SplusObjects.DigitalOutput> SOURCEENABLED;
        Crestron.Logos.SplusObjects.AnalogOutput SOURCEGROUPCOUNT;
        Crestron.Logos.SplusObjects.AnalogOutput DESTINATIONGROUPCOUNT;
        Crestron.Logos.SplusObjects.AnalogOutput SELECTEDSOURCEGROUPDEVICECOUNT;
        Crestron.Logos.SplusObjects.AnalogOutput SELECTEDDESTINATIONGROUPDEVICECOUNT;
        InOutArray<Crestron.Logos.SplusObjects.AnalogOutput> SOURCEDEVICEINPUTCOUNT;
        InOutArray<Crestron.Logos.SplusObjects.AnalogOutput> DESTINATIONDEVICEOUTPUTCOUNT;
        InOutArray<Crestron.Logos.SplusObjects.AnalogOutput> SOURCEDEVICEINPUTCLASS;
        InOutArray<Crestron.Logos.SplusObjects.AnalogOutput> DESTINATIONDEVICEOUTPUTCLASS;
        InOutArray<Crestron.Logos.SplusObjects.StringOutput> SOURCEGROUPNAME;
        InOutArray<Crestron.Logos.SplusObjects.StringOutput> DESTINATIONGROUPNAME;
        InOutArray<Crestron.Logos.SplusObjects.StringOutput> SOURCEDEVICENAME;
        InOutArray<Crestron.Logos.SplusObjects.StringOutput> SOURCEDEVICEINPUTNAME;
        InOutArray<Crestron.Logos.SplusObjects.StringOutput> DESTINATIONDEVICENAME;
        InOutArray<Crestron.Logos.SplusObjects.StringOutput> DESTINATIONDEVICEOUTPUTNAME;
        
        public override void LogosSplusInitialize()
        {
            SocketInfo __socketinfo__ = new SocketInfo( 1, this );
            InitialParametersClass.ResolveHostName = __socketinfo__.ResolveHostName;
            _SplusNVRAM = new SplusNVRAM( this );
            
            SOURCEENABLED = new InOutArray<DigitalOutput>( 400, this );
            for( uint i = 0; i < 400; i++ )
            {
                SOURCEENABLED[i+1] = new Crestron.Logos.SplusObjects.DigitalOutput( SOURCEENABLED__DigitalOutput__ + i, this );
                m_DigitalOutputList.Add( SOURCEENABLED__DigitalOutput__ + i, SOURCEENABLED[i+1] );
            }
            
            SOURCEGROUPCOUNT = new Crestron.Logos.SplusObjects.AnalogOutput( SOURCEGROUPCOUNT__AnalogSerialOutput__, this );
            m_AnalogOutputList.Add( SOURCEGROUPCOUNT__AnalogSerialOutput__, SOURCEGROUPCOUNT );
            
            DESTINATIONGROUPCOUNT = new Crestron.Logos.SplusObjects.AnalogOutput( DESTINATIONGROUPCOUNT__AnalogSerialOutput__, this );
            m_AnalogOutputList.Add( DESTINATIONGROUPCOUNT__AnalogSerialOutput__, DESTINATIONGROUPCOUNT );
            
            SELECTEDSOURCEGROUPDEVICECOUNT = new Crestron.Logos.SplusObjects.AnalogOutput( SELECTEDSOURCEGROUPDEVICECOUNT__AnalogSerialOutput__, this );
            m_AnalogOutputList.Add( SELECTEDSOURCEGROUPDEVICECOUNT__AnalogSerialOutput__, SELECTEDSOURCEGROUPDEVICECOUNT );
            
            SELECTEDDESTINATIONGROUPDEVICECOUNT = new Crestron.Logos.SplusObjects.AnalogOutput( SELECTEDDESTINATIONGROUPDEVICECOUNT__AnalogSerialOutput__, this );
            m_AnalogOutputList.Add( SELECTEDDESTINATIONGROUPDEVICECOUNT__AnalogSerialOutput__, SELECTEDDESTINATIONGROUPDEVICECOUNT );
            
            SOURCEDEVICEINPUTCOUNT = new InOutArray<AnalogOutput>( 100, this );
            for( uint i = 0; i < 100; i++ )
            {
                SOURCEDEVICEINPUTCOUNT[i+1] = new Crestron.Logos.SplusObjects.AnalogOutput( SOURCEDEVICEINPUTCOUNT__AnalogSerialOutput__ + i, this );
                m_AnalogOutputList.Add( SOURCEDEVICEINPUTCOUNT__AnalogSerialOutput__ + i, SOURCEDEVICEINPUTCOUNT[i+1] );
            }
            
            DESTINATIONDEVICEOUTPUTCOUNT = new InOutArray<AnalogOutput>( 100, this );
            for( uint i = 0; i < 100; i++ )
            {
                DESTINATIONDEVICEOUTPUTCOUNT[i+1] = new Crestron.Logos.SplusObjects.AnalogOutput( DESTINATIONDEVICEOUTPUTCOUNT__AnalogSerialOutput__ + i, this );
                m_AnalogOutputList.Add( DESTINATIONDEVICEOUTPUTCOUNT__AnalogSerialOutput__ + i, DESTINATIONDEVICEOUTPUTCOUNT[i+1] );
            }
            
            SOURCEDEVICEINPUTCLASS = new InOutArray<AnalogOutput>( 400, this );
            for( uint i = 0; i < 400; i++ )
            {
                SOURCEDEVICEINPUTCLASS[i+1] = new Crestron.Logos.SplusObjects.AnalogOutput( SOURCEDEVICEINPUTCLASS__AnalogSerialOutput__ + i, this );
                m_AnalogOutputList.Add( SOURCEDEVICEINPUTCLASS__AnalogSerialOutput__ + i, SOURCEDEVICEINPUTCLASS[i+1] );
            }
            
            DESTINATIONDEVICEOUTPUTCLASS = new InOutArray<AnalogOutput>( 400, this );
            for( uint i = 0; i < 400; i++ )
            {
                DESTINATIONDEVICEOUTPUTCLASS[i+1] = new Crestron.Logos.SplusObjects.AnalogOutput( DESTINATIONDEVICEOUTPUTCLASS__AnalogSerialOutput__ + i, this );
                m_AnalogOutputList.Add( DESTINATIONDEVICEOUTPUTCLASS__AnalogSerialOutput__ + i, DESTINATIONDEVICEOUTPUTCLASS[i+1] );
            }
            
            SOURCEGROUPNAME = new InOutArray<StringOutput>( 8, this );
            for( uint i = 0; i < 8; i++ )
            {
                SOURCEGROUPNAME[i+1] = new Crestron.Logos.SplusObjects.StringOutput( SOURCEGROUPNAME__AnalogSerialOutput__ + i, this );
                m_StringOutputList.Add( SOURCEGROUPNAME__AnalogSerialOutput__ + i, SOURCEGROUPNAME[i+1] );
            }
            
            DESTINATIONGROUPNAME = new InOutArray<StringOutput>( 8, this );
            for( uint i = 0; i < 8; i++ )
            {
                DESTINATIONGROUPNAME[i+1] = new Crestron.Logos.SplusObjects.StringOutput( DESTINATIONGROUPNAME__AnalogSerialOutput__ + i, this );
                m_StringOutputList.Add( DESTINATIONGROUPNAME__AnalogSerialOutput__ + i, DESTINATIONGROUPNAME[i+1] );
            }
            
            SOURCEDEVICENAME = new InOutArray<StringOutput>( 100, this );
            for( uint i = 0; i < 100; i++ )
            {
                SOURCEDEVICENAME[i+1] = new Crestron.Logos.SplusObjects.StringOutput( SOURCEDEVICENAME__AnalogSerialOutput__ + i, this );
                m_StringOutputList.Add( SOURCEDEVICENAME__AnalogSerialOutput__ + i, SOURCEDEVICENAME[i+1] );
            }
            
            SOURCEDEVICEINPUTNAME = new InOutArray<StringOutput>( 400, this );
            for( uint i = 0; i < 400; i++ )
            {
                SOURCEDEVICEINPUTNAME[i+1] = new Crestron.Logos.SplusObjects.StringOutput( SOURCEDEVICEINPUTNAME__AnalogSerialOutput__ + i, this );
                m_StringOutputList.Add( SOURCEDEVICEINPUTNAME__AnalogSerialOutput__ + i, SOURCEDEVICEINPUTNAME[i+1] );
            }
            
            DESTINATIONDEVICENAME = new InOutArray<StringOutput>( 100, this );
            for( uint i = 0; i < 100; i++ )
            {
                DESTINATIONDEVICENAME[i+1] = new Crestron.Logos.SplusObjects.StringOutput( DESTINATIONDEVICENAME__AnalogSerialOutput__ + i, this );
                m_StringOutputList.Add( DESTINATIONDEVICENAME__AnalogSerialOutput__ + i, DESTINATIONDEVICENAME[i+1] );
            }
            
            DESTINATIONDEVICEOUTPUTNAME = new InOutArray<StringOutput>( 400, this );
            for( uint i = 0; i < 400; i++ )
            {
                DESTINATIONDEVICEOUTPUTNAME[i+1] = new Crestron.Logos.SplusObjects.StringOutput( DESTINATIONDEVICEOUTPUTNAME__AnalogSerialOutput__ + i, this );
                m_StringOutputList.Add( DESTINATIONDEVICEOUTPUTNAME__AnalogSerialOutput__ + i, DESTINATIONDEVICEOUTPUTNAME[i+1] );
            }
            
            ID = new UShortParameter( ID__Parameter__, this );
            m_ParameterList.Add( ID__Parameter__, ID );
            
            
            
            _SplusNVRAM.PopulateCustomAttributeList( true );
            
            NVRAM = _SplusNVRAM;
            
        }
        
        public override void LogosSimplSharpInitialize()
        {
            
            
        }
        
        public UserModuleClass_TOUCHPANELSERVICE ( string InstanceName, string ReferenceID, Crestron.Logos.SplusObjects.CrestronStringEncoding nEncodingType ) : base( InstanceName, ReferenceID, nEncodingType ) {}
        
        
        
        
        const uint ID__Parameter__ = 10;
        const uint SOURCEENABLED__DigitalOutput__ = 0;
        const uint SOURCEGROUPCOUNT__AnalogSerialOutput__ = 0;
        const uint DESTINATIONGROUPCOUNT__AnalogSerialOutput__ = 1;
        const uint SELECTEDSOURCEGROUPDEVICECOUNT__AnalogSerialOutput__ = 2;
        const uint SELECTEDDESTINATIONGROUPDEVICECOUNT__AnalogSerialOutput__ = 3;
        const uint SOURCEDEVICEINPUTCOUNT__AnalogSerialOutput__ = 4;
        const uint DESTINATIONDEVICEOUTPUTCOUNT__AnalogSerialOutput__ = 104;
        const uint SOURCEDEVICEINPUTCLASS__AnalogSerialOutput__ = 204;
        const uint DESTINATIONDEVICEOUTPUTCLASS__AnalogSerialOutput__ = 604;
        const uint SOURCEGROUPNAME__AnalogSerialOutput__ = 1004;
        const uint DESTINATIONGROUPNAME__AnalogSerialOutput__ = 1012;
        const uint SOURCEDEVICENAME__AnalogSerialOutput__ = 1020;
        const uint SOURCEDEVICEINPUTNAME__AnalogSerialOutput__ = 1120;
        const uint DESTINATIONDEVICENAME__AnalogSerialOutput__ = 1520;
        const uint DESTINATIONDEVICEOUTPUTNAME__AnalogSerialOutput__ = 1620;
        
        [SplusStructAttribute(-1, true, false)]
        public class SplusNVRAM : SplusStructureBase
        {
        
            public SplusNVRAM( SplusObject __caller__ ) : base( __caller__ ) {}
            
            
        }
        
        SplusNVRAM _SplusNVRAM = null;
        
        public class __CEvent__ : CEvent
        {
            public __CEvent__() {}
            public void Close() { base.Close(); }
            public int Reset() { return base.Reset() ? 1 : 0; }
            public int Set() { return base.Set() ? 1 : 0; }
            public int Wait( int timeOutInMs ) { return base.Wait( timeOutInMs ) ? 1 : 0; }
        }
        public class __CMutex__ : CMutex
        {
            public __CMutex__() {}
            public void Close() { base.Close(); }
            public void ReleaseMutex() { base.ReleaseMutex(); }
            public int WaitForMutex() { return base.WaitForMutex() ? 1 : 0; }
        }
         public int IsNull( object obj ){ return (obj == null) ? 1 : 0; }
    }
    
    
}
