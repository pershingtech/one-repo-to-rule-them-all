﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DynamicRouting
{
    interface IRouterImplementation
    {
        //implements the syntax specific to this particular implementation to make the route
        //returns true if successful
        Task<bool> MakeRoute(object Destination, object Source, eRouteType RouteType);

        //raised for true-feedback when a destination changes source
        event EventHandler<RouteEventArgs> RouteChanged;
    }
}