﻿using System;
using System.Net.Sockets;
using System.Timers;
using System.Text;
using Crestron.SimplSharp;

namespace NetworkUtilities
{
    public class PtTcpClient : NetworkClient
    {
        //constructor
        public PtTcpClient(string IpAddress, int Port):base(IpAddress)
        {
            try
            {
                this.IpAddress = IpAddress;
                _socket = new Socket(System.Net.Sockets.AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                _endPoint = new System.Net.IPEndPoint(System.Net.IPAddress.Parse(IpAddress), Port);
                PollingTimer.Elapsed += PollingTimer_Elapsed;
                AutoConnect = true;
            }
            catch (Exception e) { CrestronConsole.PrintLine($"Error building TCP Client: {e.Message}"); }
        }

        //client objects
        private Socket _socket;
        private System.Net.IPEndPoint _endPoint;

        //polling timer objects
        private Timer PollingTimer = new Timer(10000);
        private void PollingTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            if(!_socket.Connected && _endPoint != null)
            {
                CrestronConsole.PrintLine("Socket {0} disconnected", _endPoint.Address);
                Disconnected?.Invoke(this, new EventArgs());
                if (AutoConnect)
                {
                    CrestronConsole.PrintLine("Attempting reconnect to " + _endPoint.Address);
                    Connect();
                }
            }
        }
        private void StartPolling()
        {
            PollingTimer.AutoReset = true;
            PollingTimer.Start();
        }

        //properties
        public override bool ConnectState
        { 
            get { return _socket.Connected; }
        }
        private string _ipAddress;
        public override string IpAddress
        { 
            get { return _ipAddress; }
            set
            {
                if (!String.IsNullOrEmpty(value))
                {
                    _ipAddress = value;
                    if (_endPoint != null)
                    {
                        _endPoint.Address = System.Net.IPAddress.Parse(_ipAddress);
                        if (_connect)
                        {
                            Disconnect();
                            Connect();
                        }
                    }
                }
                else CrestronConsole.PrintLine($"Invalid IP address entered: {value}");
            }
        }

        //methods
        private bool _connect;

        public override void Connect()
        {
            if (_endPoint != null)
            {
                _connect = true;
                _socket = new Socket(SocketType.Stream, ProtocolType.Tcp);
                SocketAsyncEventArgs ConnectArgs = new SocketAsyncEventArgs()
                {
                    RemoteEndPoint = _endPoint
                };
                ConnectArgs.Completed += ConnectArgs_Completed;
                _socket.ConnectAsync(ConnectArgs);
            }
        }
        public override void Disconnect()
        {
            _connect = false;
            if (_endPoint != null)
            {
                SocketAsyncEventArgs DisconnectArgs = new SocketAsyncEventArgs()
                {
                    RemoteEndPoint = _endPoint
                };
                //PollingTimer.Stop();
                _socket.Close();
                _socket = new Socket(System.Net.Sockets.AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            }
        }
        private void ConnectArgs_Completed(object sender, SocketAsyncEventArgs e)
        {
            if (e.ConnectSocket != null)
            {
                if (e.ConnectSocket.Connected)
                {
                    _socket = e.ConnectSocket;

                    StateObject state = new StateObject();
                    state.workSocket = _socket;
                    _socket.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0, new AsyncCallback(ReceiveCallback), state);

                    if (Debug) { CrestronConsole.PrintLine(IpAddress + " connected"); }

                    Connected?.Invoke(this, new EventArgs());
                    StartPolling();
                }
                else Connect();
            }
            else
            {
                //Console.WriteLine("e.ConnectSocket was null");
                CrestronConsole.PrintLine($"Connection attempt to {IpAddress} timed out");
                if(_connect) Connect();
            }
        }
        public override void SendString(string Data)
        {
            try
            {
                if (_endPoint != null)
                {
                    if (Debug) CrestronConsole.PrintLine("{0} Tx: {1}", _endPoint.Address, Data);
                    byte[] DataBytes = Encoding.ASCII.GetBytes(Data);
                    _socket.Send(DataBytes);
                }
            }
            catch (System.Net.Sockets.SocketException e)
            {
                if (e.SocketErrorCode != SocketError.Success)
                {
                    //ConnectState = false;
                    CrestronConsole.PrintLine("Socket error: {0}", e.Message);
                    CrestronConsole.PrintLine("ConnectState: {0}", ConnectState);
                    Disconnected?.Invoke(this, new EventArgs());
                }
            }
        }
        public override void SendBytes(byte[] Data)
        {
            try
            {
                if (_endPoint != null)
                {
                    if (Debug) CrestronConsole.PrintLine("{0} Tx: {1}", _endPoint.Address, BitConverter.ToString(Data));
                    _socket.Send(Data);
                }
            }
            catch (System.Net.Sockets.SocketException e)
            {
                if (e.SocketErrorCode != SocketError.Success)
                {
                    //ConnectState = false;
                    CrestronConsole.PrintLine($"{IpAddress} Socket error: {e.Message}");
                    //Console.WriteLine("ConnectState: {0}", ConnectState);
                    Disconnected?.Invoke(this, new EventArgs());
                }
            }
        }


        //events
        public override event EventHandler Connected;
        public override event EventHandler Disconnected;
        public override event EventHandler<DataReceivedArgs> DataReceived;

        //asynchronous receive stuff
        private class StateObject
        {
            // Client socket.  
            public Socket workSocket = null;
            // Size of receive buffer.  
            public const int BufferSize = 10000;
            // Receive buffer.  
            public byte[] buffer = new byte[BufferSize];
            // Received data string.  
            public string s;
        }
        private void ReceiveCallback(IAsyncResult ar)
        {
            try
            {
                StateObject state = (StateObject)ar.AsyncState;
                Socket client = state.workSocket;

                int bytesRead = client.EndReceive(ar);
                if (bytesRead > 0)
                {
                    try
                    {
                        state.s = String.Empty;

                        byte[] TrimmedBuffer = TrimZeros(state.buffer);

                        try { state.s = Encoding.ASCII.GetString(state.buffer, 0, TrimmedBuffer.Length); }
                        catch { CrestronConsole.PrintLine($"{IpAddress} error converting bytes received to string"); }

                        //CrestronConsole.PrintLine($"state.s: {state.s}");
                        if (Debug)
                        {
                            CrestronConsole.PrintLine("{0} Rx (string): {1}", _endPoint.Address, state.s);
                            CrestronConsole.PrintLine("{0} Rx (bytes): {1}\n", _endPoint.Address, BitConverter.ToString(TrimmedBuffer));
                        }
                        DataReceived?.Invoke(this, new DataReceivedArgs()
                        {
                            DataBytes = TrimmedBuffer,
                            DataString = state.s
                        });


                    }
                    catch (Exception e)
                    {
                        CrestronConsole.PrintLine("Error converting buffer to string: " + e.Message);
                    }

                    Array.Clear(state.buffer, 0, state.buffer.Length);
                    client.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
                           new AsyncCallback(ReceiveCallback), state);
                }
            }
            catch (Exception e)
            {
                CrestronConsole.PrintLine($"{IpAddress} Error receiving data: " + e.Message);
                CrestronConsole.PrintLine(e.StackTrace);
            }
        }
        private byte[] TrimZeros(byte[] packet)
        {
            var i = packet.Length - 1;
            while (packet[i] == 0)
            {
                --i;
            }
            var temp = new byte[i + 1];
            Array.Copy(packet, temp, i + 1);
            return temp;
        }
    }
}
