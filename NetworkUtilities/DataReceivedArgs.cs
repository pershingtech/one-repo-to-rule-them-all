﻿using System;

namespace NetworkUtilities
{
    public class DataReceivedArgs : EventArgs
    {
        public byte[] DataBytes;
        public string DataString;
    }
}