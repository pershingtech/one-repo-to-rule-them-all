﻿using System;
using System.Text;
using Crestron.SimplSharp;
using Crestron.SimplSharp.CrestronSockets;

namespace NetworkUtilities
{
    public class PtUdpClient : NetworkClient
    {
        //constructor
        public PtUdpClient(string IpAddress, int Port) : base (IpAddress)
        {
            _client = new UDPServer(IpAddress, Port, 10000);
            _client.ReceiveDataAsync(DataReceiveCallback);
        }

        //client objects
        private UDPServer _client;
        private void DataReceiveCallback(UDPServer server, int BytesReceived)
        {
            CrestronConsole.PrintLine("{0} Rx: {1}", IpAddress, Encoding.ASCII.GetString(server.IncomingDataBuffer));
            
            Connected?.Invoke(this, new EventArgs());
            DataReceived?.Invoke(this, new DataReceivedArgs()
            {
                DataBytes = server.IncomingDataBuffer,
                DataString = Encoding.ASCII.GetString(server.IncomingDataBuffer)
            });

            _client.ReceiveDataAsync(DataReceiveCallback);
        }

        //properties
        private bool _connectedHasBeenRisen;
        public override bool ConnectState
        {
            get { return _client.ServerStatus == SocketStatus.SOCKET_STATUS_CONNECTED; }
        }
        public override string IpAddress 
        {
            get { return (_client != null) ?_client.AddressToAcceptConnectionFrom : string.Empty; }
            set { if(_client != null) _client.AddressToAcceptConnectionFrom = value; }
        }

        //methods
        public override void Connect()
        {
            try
            {
                _client.EnableUDPServer();
                _client.ReceiveDataAsync(DataReceiveCallback);
            }
            catch(Exception e)
            {
                CrestronConsole.PrintLine("Error enabling UDP server: " + e.Message);
            }
        }
        public override void Disconnect()
        {
            try
            {
                _client.DisableUDPServer();
                _connectedHasBeenRisen = false;
                Disconnected?.Invoke(this, new EventArgs());
            }
            catch (Exception e)
            {
                CrestronConsole.PrintLine("Error enabling UDP server: " + e.Message);
            }
        }
        public override void SendBytes(byte[] Data)
        {
            try
            {
                _client.SendDataAsync(Data, Data.Length, SendDataCallback);
            }
            catch (Exception e)
            {
                CrestronConsole.PrintLine("Error sending byte array: " + e.Message);
                _connectedHasBeenRisen = false;
                Disconnected?.Invoke(this, new EventArgs());
            }
        }
        public override void SendString(string Data)
        {
            try
            {
                CrestronConsole.PrintLine("{0} Tx: {1}", IpAddress, Data);
                byte[] DataBytes = Encoding.ASCII.GetBytes(Data);
                _client.SendDataAsync(DataBytes, DataBytes.Length, SendDataCallback);
            }
            catch (Exception e)
            {
                CrestronConsole.PrintLine("Error sending string: " + e.Message);
                _connectedHasBeenRisen = false;
                Disconnected?.Invoke(this, new EventArgs());
            }
        }

        private void SendDataCallback(UDPServer Server, int BytesSent)
        {

        }
        
        //events
        public override event EventHandler Connected;
        public override event EventHandler Disconnected;
        public override event EventHandler<DataReceivedArgs> DataReceived;
    }
}