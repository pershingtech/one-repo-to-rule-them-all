﻿using System;
using Crestron.SimplSharp;

namespace NetworkUtilities
{
    public abstract class NetworkClient
    {
        //constructor
        public NetworkClient(string ipAddress)
        {
            try
            {
                IpAddress = ipAddress;
                string formattedIpAddress = IpAddress.Replace(".", string.Empty);
                CrestronConsole.AddNewConsoleCommand(debugCallback, "debug" + formattedIpAddress, $"Toggles send & receive debugging for device at {IpAddress}", ConsoleAccessLevelEnum.AccessOperator);
                Debug = false;
            }
            catch (Exception e) { CrestronConsole.PrintLine("Error in network client constructor: " + e.Message + e.StackTrace); }
        }

        //debug console command
        public bool Debug { get; set; }
        protected void debugCallback(string Parameters)
        {
            Debug = !Debug;
            CrestronConsole.PrintLine(String.Format("Debugging {0} {1}", IpAddress, Debug));
        }

        //properties
        public abstract bool ConnectState { get; }
        public virtual bool AutoConnect { get; set; }
        public abstract string IpAddress { get; set; }

        //methods
        public abstract void Connect();
        public abstract void Disconnect();
        public abstract void SendString(string Data);
        public abstract void SendBytes(byte[] Data);

        //events
        public abstract event EventHandler Connected;
        public abstract event EventHandler Disconnected;
        public abstract event EventHandler<DataReceivedArgs> DataReceived;
    }
}
