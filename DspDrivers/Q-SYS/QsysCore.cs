﻿using System;
using System.Linq;
using System.Timers;
using System.Net.Sockets;

namespace DspDrivers
{
    public class QsysCore : DspCore
    {
        public QsysCore(string IpAddress) : base(IpAddress, 1702, ProtocolType.Tcp, '\n')
        {

        }
        public QsysCore(string IpAddress, string UserName, string Password) : base (IpAddress, 1702, ProtocolType.Tcp, '\n')
        {
            this.UserName = UserName;
            this.Password = Password;
        }
        public string UserName { set; get; }
        public string Password { set; get; }

        private Timer KeepAlive;
        private void KeepAlive_Elapsed(object sender, ElapsedEventArgs e)
        {
            Client.SendString("sg\n");
        }

        protected override void TcpClient_Connected(object sender, EventArgs e)
        {
            //create control group
            Client.SendString("cgc 1\n");

            //setup unsolicited feedback from the control group
            //poll interval 250ms
            Client.SendString("cgsna 1 250\n");

            //add each control in the DspControl List to the control group
            SubscribeToUpdates();

            //start the keepalive timer
            KeepAlive = new Timer(10000);
            KeepAlive.AutoReset = true;
            KeepAlive.Elapsed += KeepAlive_Elapsed;
            KeepAlive.Start();
        }
        protected override void TcpClient_Disconnected(object sender, EventArgs e)
        {
            KeepAlive?.Stop();
        }
        
        //control creation
        protected override void CreateControl<T>(string Identifier)
        {
            QsysControl<T> control = new QsysControl<T>(Identifier);
            control.SendCommand += Control_SendCommand;
            DspControls.Add(control);
        }
        protected override void SubscribeToControlUpdate(DspControl dspControl)
        {
            Client.SendString("cga 1 " + dspControl.Identifier + "\n");
        }

        //parsing response from device
        protected override void ParseGatheredResponse(string Response)
        {
            //Example response string:
            //cv "GainGain" "-27.6dB" -27.6 0.603333
            string[] ResponseSections = Response.Split(' '); //split the response into sections divided by spaces
            bool isDeviceStatus = (ResponseSections[0] == "cv"); //make sure the response is a control value update

            if (isDeviceStatus)
            {
                string ControlIdentifier = ResponseSections[1];

                //remove leading and trailing "'s
                ControlIdentifier = ControlIdentifier.Remove(0, 1);
                ControlIdentifier = ControlIdentifier.Remove(ControlIdentifier.Length - 1, 1);

                //find the control in the list that matches the name sent
                var UpdatedControl = DspControls.FirstOrDefault(x => x.Identifier == ControlIdentifier);

                //if there is a match, call the ParseResponse method on the control
                UpdatedControl?.ParseResponse(Response);
            }
        }
    }
}
