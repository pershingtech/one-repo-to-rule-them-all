﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DspDrivers
{
    public class QsysControl<T> : DspControl<T>
    {
        //constructor
        public QsysControl(string Identifier) : base(Identifier)
        {

        }
        
        //parse response strings & raise ValueChanged event
        protected override void ParseBoolStatus(string Response)
        {
            //Example response string:
            //cv "GainGain" "-27.6dB" -27.6 0.603333
            string[] ResponseSections = Response.Split(' '); //split the response into sections divided by spaces
            int BoolState = Int16.Parse(ResponseSections[3]);
            bool State = (BoolState > 0);

            OnValueChanged(new ValueChangeArgs()
            {
                ControlType = TypeCode.Boolean,
                BoolValue = State
            }) ;
        }
        protected override void ParseStringStatus(string Response)
        {
            //Example response string:
            //cv "PhoneDialString" "123456" 0 0
            string[] ResponseSections = Response.Split(' '); //split the response into sections divided by spaces
            string Value = ResponseSections[2];
            Value = Value.Remove(0, 1); //remove leading "
            Value = Value.Remove(Value.Length - 1, 1); //remove trailing "

            OnValueChanged(new ValueChangeArgs()
            {
                ControlType = TypeCode.String,
                StringValue = Value
            });
        }
        protected override void ParseUshortStatus(string Response)
        {
            //Example response string:
            //cv "GainGain" "-27.6dB" -27.6 0.603333
            string[] ResponseSections = Response.Split(' '); //split the response into sections divided by spaces
            ushort uValue = UInt16.Parse(ResponseSections[3]);

            OnValueChanged(new ValueChangeArgs()
            {
                ControlType = TypeCode.UInt16,
                UshortValue = uValue
            }) ;
        }
        protected override void ParseFloatStatus(string Response)
        {
            //Example response string:
            //cv "GainGain" "-27.6dB" -27.6 0.603333
            string[] ResponseSections = Response.Split(' '); //split the response into sections divided by spaces
            float ValuePercentage = float.Parse(ResponseSections[4]);
            float fValueScaled = ValuePercentage * UInt16.MaxValue;

            ushort uValueScaled = (ushort)Math.Round(fValueScaled);
            OnValueChanged(new ValueChangeArgs()
            {
                ControlType = TypeCode.UInt16,
                UshortValue = uValueScaled,
                FloatValue = ValuePercentage
            }) ;
        }

        //build command strings and raise SendCommand event
        protected override void SetBoolValue(bool Value)
        {
            int iBool = Value ? 1 : 0;
            string Command = String.Format("csv {0} {1}\n", Identifier, iBool);
            OnSendCommand(new StringEventArgs()
            {
                StringValue = Command
            }) ;
        }
        protected override void SetStringValue(string Value)
        {
            string Command = String.Format("css {0} {1}\n", Identifier, Value);
            OnSendCommand(new StringEventArgs()
            {
                StringValue = Command
            }) ;
        }
        protected override void SetUshortValue(ushort Value)
        {
            string Command = String.Format("csv {0} {1}\n", Identifier, Value);
            OnSendCommand(new StringEventArgs()
            {
                StringValue = Command
            }) ;
        }
        protected override void SetFloatValue(float Value)
        {
            string Command = String.Format("csp {0} {1}\n", Identifier, Value.ToString("0.00"));
            OnSendCommand(new StringEventArgs()
            {
                StringValue = Command
            });
        }
    }
}
