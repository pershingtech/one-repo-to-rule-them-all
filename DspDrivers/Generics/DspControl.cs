﻿using System;

namespace DspDrivers
{
    public abstract class DspControl
    {
        public string Identifier { set; get; }
        public abstract Type Type { get; }
        internal abstract void ParseResponse(string Response);

        internal event EventHandler<StringEventArgs> SendCommand;
        protected void BaseSendCommand(StringEventArgs e)
        {
            SendCommand?.Invoke(this, e);
        }
    }
    public abstract class DspControl<T> : DspControl
    {
        //control must have an identifier name
        public DspControl(string Identifier)
        {
            this.Identifier = Identifier;
        }

        public override Type Type
        {
            get { return typeof(T); }
        }

        //when Value is set, we switch on what data type this control is
        //depending on which type it is, we call one of 3 methods which
        //compute the corresponding command to the DSP
        protected T _value;
        public T Value
        { 
            get { return _value; }
            set
            {
                _value = value;
                switch (Type.GetTypeCode(typeof(T)))
                {
                    case TypeCode.Boolean:
                        {
                            SetBoolValue((bool)(object)_value);
                            break;
                        }
                    case TypeCode.UInt16:
                        {
                            SetUshortValue((ushort)(object)_value);
                            break;
                        }
                    case TypeCode.String:
                        {
                            SetStringValue((string)(object)_value);
                            break;
                        }
                    case TypeCode.Single:
                        {
                            SetFloatValue((float)(object)_value);
                            break;
                        }
                    default:
                        {
                            throw new NotImplementedException();
                        }
                }
            }
        }

        //each method must generate the corresponding command for each
        //type and then raise the SendCommand event
        protected abstract void SetBoolValue(bool Value);
        protected abstract void SetUshortValue(ushort Value);
        protected abstract void SetStringValue(string Value);
        protected abstract void SetFloatValue(float Value);

        protected void OnSendCommand(StringEventArgs e)
        {
            BaseSendCommand(e);
        }

        //each method should parse the value from the DSP's response
        //string and then raise the ValueChanged event
        internal override void ParseResponse(string Response)
        {
            switch (Type.GetTypeCode(typeof(T)))
            {
                case TypeCode.Boolean:
                    {
                        ParseBoolStatus(Response);
                        break;
                    }
                case TypeCode.UInt16:
                    {
                        ParseUshortStatus(Response);
                        break;
                    }
                case TypeCode.String:
                    {
                        ParseStringStatus(Response);
                        break;
                    }
                case TypeCode.Single:
                    {
                        ParseFloatStatus(Response);
                        break;
                    }
                default: 
                    {
                        throw new NotImplementedException();
                    }
            }
        }
        
        protected abstract void ParseBoolStatus(string Response);
        protected abstract void ParseUshortStatus(string Response);
        protected abstract void ParseStringStatus(string Response);
        protected abstract void ParseFloatStatus(string Response);

        public event EventHandler<ValueChangeArgs> ValueChanged;
        protected void OnValueChanged(ValueChangeArgs e)
        { 
            ValueChanged?.Invoke(this, e);
        }

        //utility method to convert Ushort value (from a touch panel
        //fader, for example) to a float % for a gain block
        public static float ScaleUshort(ushort Value)
        {
            return ((float)Value / UInt16.MaxValue);
        }
    }
}