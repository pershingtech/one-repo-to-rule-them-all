﻿using System;

namespace DspDrivers
{
    public class ValueChangeArgs : EventArgs
    {
        public TypeCode ControlType;
        public bool BoolValue;
        public ushort UshortValue;
        public string StringValue;
        public float FloatValue;
    }
    public class StringEventArgs : EventArgs
    {
        public string StringValue;
    }
}
