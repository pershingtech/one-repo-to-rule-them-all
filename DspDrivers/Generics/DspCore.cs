﻿using NetworkUtilities;
using System.Collections.Generic;
using System;
using System.Net.Sockets;
using System.Timers;
using System.Text;
using Crestron.SimplSharp;

namespace DspDrivers
{
    public abstract class DspCore
    {
        //create client object and subscribe to its connected event
        public DspCore(string IpAddress, ushort Port, ProtocolType Protocol, char ResponseDelimeter)
        {
            switch(Protocol)
            {
                case ProtocolType.Tcp: { Client = new PtTcpClient(IpAddress, (int)Port); break; }
                case ProtocolType.Udp: { Client = new PtUdpClient(IpAddress, (int)Port); break; }
            }

            this.IpAddress = IpAddress;

            Client.Connected += TcpClient_Connected;
            Client.Disconnected += TcpClient_Disconnected;
            Client.DataReceived += TcpClient_DataReceived;

            this.ResponseDelimeter = ResponseDelimeter;
            ResponseTimeout.Elapsed += ResponseTimeout_Elapsed;
        }

        public string IpAddress { get; private set; }

        private bool _connect;
        public bool Connect
        {
            get { return Client.ConnectState; }
            set
            {
                _connect = value;
                if(_connect)
                {
                    Client.AutoConnect = true;
                    Client.Connect();
                }
                else
                {
                    Client.AutoConnect = false;
                    Client.Disconnect();
                }
            }
        }

        //upon congnection, subscribe to updates for all my DspControls
        //virtual because some clients may require login upon connection
        protected virtual void TcpClient_Connected(object sender, EventArgs e)
        {
            SubscribeToUpdates();
        }

        //inherrited classes may choose to override this depending on their requirements
        //for example, Q-SYS will stop its keep-alive Timer upon disconnection
        protected virtual void TcpClient_Disconnected(object sender, EventArgs e) { }

        //when data is received on the TCP client, call the parse response method
        private void TcpClient_DataReceived(object sender, DataReceivedArgs e)
        {
            ParseDeviceResponse(e.DataString);
        }

        protected NetworkClient Client;
        protected List<DspControl> DspControls = new List<DspControl>();

        //method may need to be overriden depending on the DSP's requirements
        //for instance, Q-Sys requires creating a "change group" before
        //named controls can be added to the group
        protected virtual void SubscribeToUpdates()
        {
            foreach (DspControl dspControl in DspControls)
                SubscribeToControlUpdate(dspControl);
            
        }

        //method must set up asynch updates on the DSP for the dspControl parameter
        //for example, Q-SYS uses the "cga" command
        protected abstract void SubscribeToControlUpdate(DspControl dspControl);

        //method allows controls to be added to this DSP core
        //CreateControl<T> must be overriden in derrived class because
        //DspControl<T> is an abstract class and cannot be instantiated and added
        //to the collection
        public virtual void CreateControl(DspControl dspControl)
        {
            dspControl.SendCommand += Control_SendCommand;
            if (Client.ConnectState)
                SubscribeToControlUpdate(dspControl);
            DspControls.Add(dspControl);
        }

        //CreateControl<T> should subscribe to the DspControl<T>.SendCommand
        //event for each control created
        protected abstract void CreateControl<T>(string Identifier);

        //event handler to send commands from controls in DspControls List to the client object
        protected void Control_SendCommand(object sender, StringEventArgs e)
        {
            try
            {
                //Console.WriteLine("Sending: " + e.StringValue);
                Client.SendString(e.StringValue);
            }
            catch(Exception ex) { CrestronConsole.PrintLine("Error sending DSP command: " + ex.Message); }
        }

        /*
         * In testing it was discovered that Q-SYS would send one full response
         * string PLUS the first few characters of the next in one TCP packet,
         * which caused parsing errors and missed responses. The ResponseTimer
         * is reset everytime data is received, with the data appended to the
         * GatheredResponse string builder. Only after the ResponseTimeout
         * expires are the responses parsed.
         */
        private const int TimeoutInterval = 100;
        private char ResponseDelimeter;

        Timer ResponseTimeout = new Timer(TimeoutInterval);
        StringBuilder GatheredResponse = new StringBuilder();

        protected virtual void ParseDeviceResponse(string Response)
        {
            GatheredResponse.Append(Response);
            ResponseTimeout.Stop();
            ResponseTimeout.Start();
        }
        protected virtual void ResponseTimeout_Elapsed(object sender, ElapsedEventArgs e)
        {
            if(GatheredResponse.Length > 0)
            {
                string[] Responses = GatheredResponse.ToString().Split(ResponseDelimeter);
                GatheredResponse.Clear();
                ResponseTimeout.Stop();

                foreach (string s in Responses)
                    ParseGatheredResponse(s); 
            }
        }

        //method must parse the response from the device and pass the response to the
        //corresponding DspControl in the List for parsing
        protected abstract void ParseGatheredResponse(string Response);
    }
}