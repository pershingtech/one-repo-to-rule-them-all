﻿using System;
using System.Net.Sockets;
using System.Linq;
using Crestron.SimplSharp;

namespace DspDrivers
{
    public class SymetrixCore : DspCore
    {
        public SymetrixCore(string IpAddress) : base(IpAddress, 48631, ProtocolType.Tcp, '\r')
        {

        }

        protected override void SubscribeToUpdates()
        {
            Client.SendString("PU 1\r"); //first globally turn "Push" commands on
            Client.SendString("PUI 250\r"); //set push interval to a quarter-second

            base.SubscribeToUpdates();
        }
        protected override void SubscribeToControlUpdate(DspControl dspControl)
        {
            Client.SendString("PUE " + dspControl.Identifier + "\r");
        }

        protected override void CreateControl<T>(string Identifier)
        {
            SymetrixControl<T> control = new SymetrixControl<T>(Identifier);
            control.SendCommand += Control_SendCommand;
            DspControls.Add(control);
        }

        protected override void ParseGatheredResponse(string Response)
        {
            if (Response.IndexOf('#') == 0) //response is a control update
            {
                //example response string:
                //#00324=00128<CR>
                //{CS 00001 65535}
                int startPos = 1;
                int endPos = Response.IndexOf("=");
                string ControlIdentifier = Response.Substring(startPos, endPos - startPos);

                //find the control in the list that matches the name sent
                var UpdatedControl = DspControls.FirstOrDefault(x => x.Identifier == ControlIdentifier);

                //if there is a match, call the ParseResponse method on the control
                UpdatedControl?.ParseResponse(Response);
            }
        }
    }
}
