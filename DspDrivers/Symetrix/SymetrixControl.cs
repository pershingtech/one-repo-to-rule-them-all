﻿using Crestron.SimplSharp;
using Crestron.SimplSharp.WebScripting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DspDrivers
{
    public class SymetrixControl<T> : DspControl<T>
    {
        //constants
        private const int BoolOn = 65535;
        private const int BoolOff = 0;

        //constructor
        public SymetrixControl(string Identifier) : base(Identifier)
        {
                
        }
        public SymetrixControl(string Identifier, ushort ValueCount) : base(Identifier)
        {
            this.ValueCount = ValueCount;
        }

        /*
         * Symetrix does input selection blocks weird. The selected input 
         * number is done by setting a value as a ushort percentage of total
         * number of inputs. The formula used is as follows:
         * ValueToSend = (Input Number - 1)*65535/(ValueCount - 1)
         * 
         * If the ValueCount property has a value, the SetUshortValue method
         * will compute the value to send according to this formula, otherwise
         * if will send the raw ushort value passed to the method.
         * 
         * Similarly if ValueCount has a value, the ParseUshortStatus method
         * will compute the updated value according to the percentage of 65535
         * and raise that value in the event
         */
        public ushort ValueCount { set; get; }

        //parse response strings & raise ValueChanged event
        protected override void ParseBoolStatus(string Response)
        {
            var Value = ParseReturnedValue(Response);
            OnValueChanged(new ValueChangeArgs()
            {
                ControlType = TypeCode.Boolean,
                BoolValue = (Value == BoolOn)
            }) ;
        }
        protected override void ParseFloatStatus(string Response)
        {
            var Value = ParseReturnedValue(Response);
            float fValue = (float)Value / ushort.MaxValue;
            OnValueChanged(new ValueChangeArgs()
            {
                ControlType = TypeCode.Single,
                FloatValue = fValue,
                UshortValue = Value
            });
        }
        protected override void ParseStringStatus(string Response)
        {
            throw new NotImplementedException();
        }
        protected override void ParseUshortStatus(string Response)
        {
            var Value = ParseReturnedValue(Response);
            if (ValueCount > 0)
                Value = (ushort)(Value / ushort.MaxValue);
            OnValueChanged(new ValueChangeArgs()
            {
                ControlType = TypeCode.UInt16,
                UshortValue = Value
            });
        }

        private ushort ParseReturnedValue(string Response)
        {
            //example response: #10000=65535<CR>
            int startPos = Response.IndexOf("=") + 1;
            int endPos = startPos + 5;
            string sValue = Response.Substring(startPos, endPos - startPos);

            ushort Value;
            bool Result = UInt16.TryParse(sValue, out Value);
            if (Result)
                return Value;
            else throw new Exception(String.Format("Error parsing Symetrix value response \"{0}\"", sValue));
        }

        //build command strings & raise SendCommand event
        protected override void SetBoolValue(bool Value)
        {
            string Command;
            if (Value)
                Command = String.Format("$e CS {0} {1}\r", Identifier, BoolOn);
            else Command = String.Format("$e CS {0} {1}\r", Identifier, BoolOff);

            OnSendCommand(new StringEventArgs()
            {
                StringValue = Command
            });
        }
        protected override void SetFloatValue(float Value)
        {
            float uScaled = Value * ushort.MaxValue; //multiply float by 65535 to get the relative ushort value
            ushort uValue = (ushort)Math.Round(uScaled); //convert to an actual ushort

            string Command = String.Format("$e CS {0} {1}\r", Identifier, uValue);
            OnSendCommand(new StringEventArgs()
            {
                StringValue = Command
            });
        }
        protected override void SetStringValue(string Value)
        {
            throw new NotImplementedException();
        }
        protected override void SetUshortValue(ushort Value)
        {
            ushort ValueToSend;

            //see comments above `public ushort ValueCount`
            if (ValueCount > 0)
                ValueToSend = (ushort)(((Value - 1) * ushort.MaxValue) / (ValueCount - 1));
            else ValueToSend = Value;
            
            string Command = String.Format("$e CS {0} {1}\r", Identifier, ValueToSend);
            OnSendCommand(new StringEventArgs()
            {
                StringValue = Command
            });
        }
    }
}
