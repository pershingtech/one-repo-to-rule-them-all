﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Threading.Tasks;
using System.Text;
using System.Linq;
using System.Timers;

namespace Utilities
{
    public class DiscoveredCrestronDevice
    {
        public string ModelNumber { get; private set; }
        public string SerialNumber { get; private set; }
        public string Hostname { get; private set; }
        public IPAddress IpAddress { get; private set; }
        public PhysicalAddress MacAddress { get; private set; }
        public string FirmwareVersion { get; private set; }

        public static Task<List<DiscoveredCrestronDevice>> DiscoverDevices()
        {
            return Task.Run(() => DiscoverDevicesTask());
        }

        private static List<DiscoveredCrestronDevice> DiscoverDevicesTask()
        {
            //create timer and UdpClient for sending broadcast and listening to responses
            Timer ListenTimer = new Timer(5000);

            int ListenPort = 41794;
            UdpClient listener = new UdpClient(ListenPort);
            listener.Client.ReceiveTimeout = 5000;

            //create a temporary IpEndpoint, this will be assigned with each responses to the UDP broadcast
            IPEndPoint discoveredEnpoint = new IPEndPoint(IPAddress.Any, ListenPort);

            //data bytes captured by WireShark from Device Discovery broadcast
            byte[] dataBytes = { 0x14, 0x00, 0x00, 0x00, 0x01, 0x04, 0x00, 0x03,
                 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                 0x00, 0x00 };

            Console.WriteLine("Running Auto-Discovery...");

            //create collection to store results
            Dictionary<IPEndPoint, byte[]> TempResults = new Dictionary<IPEndPoint, byte[]>();

            //create a list of parsed results, will be populated in the "finally" block
            List<DiscoveredCrestronDevice> tempList = new List<DiscoveredCrestronDevice>();

            //send the network broadcast
            listener.Send(dataBytes, dataBytes.Length, "255.255.255.255", ListenPort);

            //start listening
            ListenTimer.Start();
            try
            {
                while (ListenTimer.Enabled)
                {
                    byte[] dataRx = listener.Receive(ref discoveredEnpoint);
                    TempResults.Add(discoveredEnpoint, dataRx);
                }
            }
            catch
            {
                //catch is thrown when the UdpListener times out, don't need to do anything here
            }
            finally
            {
                //after the catch block runs, this block runs
                var localHost = Dns.GetHostEntry(Dns.GetHostName());

                //iterate through all responses, parse result if the responses is not from this system's IP address
                foreach (KeyValuePair<IPEndPoint, byte[]> result in TempResults)
                    if (!localHost.AddressList.Contains(result.Key.Address))
                        tempList.Add(ParseResult(result.Key, result.Value));

            }

            return tempList;
        }
        private static DiscoveredCrestronDevice ParseResult(IPEndPoint endPoint, byte[] data)
        {
            try
            {
                DiscoveredCrestronDevice tempDevice = new DiscoveredCrestronDevice();
                tempDevice.IpAddress = endPoint.Address;

                /*******parse model number********/
                int firstByte = 266;

                //model number ends with " "
                byte byteToFind = Encoding.ASCII.GetBytes(" ")[0];

                int lastByte = Array.IndexOf(data, byteToFind, firstByte);
                byte[] parsedBytes = new byte[lastByte - firstByte];
                Array.Copy(data, firstByte, parsedBytes, 0, lastByte - firstByte);

                tempDevice.ModelNumber = Encoding.Default.GetString(parsedBytes);

                /****parse hostname****/
                //parse hostname
                firstByte = 10;
                byteToFind = Convert.ToByte(0);
                lastByte = Array.IndexOf(data, byteToFind, firstByte);

                parsedBytes = new byte[lastByte - firstByte];
                Array.Copy(data, firstByte, parsedBytes, 0, lastByte - firstByte);

                tempDevice.Hostname = Encoding.Default.GetString(parsedBytes);

                /*****parse firmware*****/
                byte bracketByte = Encoding.ASCII.GetBytes("[")[0];
                byte spaceByte = Encoding.ASCII.GetBytes(" ")[0];
                firstByte = Array.IndexOf(data, bracketByte) + 1;
                lastByte = Array.IndexOf(data, spaceByte, firstByte);
                parsedBytes = new byte[lastByte - firstByte];
                Array.Copy(data, firstByte, parsedBytes, 0, lastByte - firstByte);

                tempDevice.FirmwareVersion = Encoding.Default.GetString(parsedBytes);

                /****parse serial number*****/
                byte poundByte = Encoding.ASCII.GetBytes("#")[0];
                byte closeBracketByte = Encoding.ASCII.GetBytes("]")[0];
                firstByte = Array.IndexOf(data, poundByte) + 1;
                lastByte = Array.IndexOf(data, closeBracketByte);
                parsedBytes = new byte[lastByte - firstByte];
                Array.Copy(data, firstByte, parsedBytes, 0, lastByte - firstByte);

                tempDevice.SerialNumber = Encoding.Default.GetString(parsedBytes);

                /****parse MAC*****/
                byte macStart = Encoding.ASCII.GetBytes("@")[0];
                firstByte = Array.IndexOf(data, macStart) + 3;
                lastByte = firstByte + 12;
                parsedBytes = new byte[lastByte - firstByte];
                Array.Copy(data, firstByte, parsedBytes, 0, lastByte - firstByte);
                string sMac = Encoding.Default.GetString(parsedBytes).ToUpper();

                tempDevice.MacAddress = PhysicalAddress.Parse(sMac);

                return tempDevice;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            return new DiscoveredCrestronDevice();
        }
    }
}
