﻿using Crestron.SimplSharp;
using System.Net.Mail;
using System.Net;
using System.Collections.Generic;
using System;

namespace Utilities
{
    public class SmtpSender
    {
        public string Server { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string FromSender { get; set; }
        public string ToRecipient { get; set; }
        public string CarbonCopy { get; set; }
        public string Subject { get; set; }

        //public void SendMail(string Message)
        //{
        //    var result = CrestronMailFunctions.SendMail(
        //        Server,
        //        UserName,
        //        Password,
        //        FromSender,
        //        ToRecipient,
        //        CarbonCopy,
        //        Subject,
        //        Message);

        //    if (result != CrestronMailFunctions.SendMailErrorCodes.SMTP_OK)
        //        CrestronConsole.PrintLine($"Error sending email message, error code {result}");
        //}

        private List<string> Recipients = new List<string>();

        public void AddRecipients(string Recipient)
        {
            Recipients.Add(Recipient);
        }

        public void SendMail(string Message)
        {
            SmtpClient smtpClient = new SmtpClient(Server)
            {
                Credentials = new NetworkCredential(UserName, Password),
                EnableSsl = false
            };

            MailMessage message = new MailMessage()
            {
                From = new MailAddress(FromSender),
                Subject = this.Subject,
                Body = Message,
                IsBodyHtml = false
            };
            foreach (string s in Recipients)
                message.To.Add(s);
            try
            {
                smtpClient.Send(message);
            }
            catch(Exception e)
            {
                CrestronConsole.PrintLine(e.Message);
            }
        }
    }
}
