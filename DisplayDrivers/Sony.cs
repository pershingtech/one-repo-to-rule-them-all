﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NetworkUtilities;

namespace DisplayDrivers
{
    class Sony : IDisplay
    {
        private PtTcpClient tcpClient;
        
        private bool _power;
        public bool Power {
            get { return _power; }
            set { SetPower(value); }
        }
        public ushort Volume { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public eDisplayInput Input { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public bool SupportsQuad { get; private set; }

        private bool _quadDisplay;
        public bool QuadDisplay
        {
            get { return _quadDisplay; }
            set { SetQuadview(value); }
        }
        public string IpAddress { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public event EventHandler<PowerEventArgs> PowerStateChanged;
        public event EventHandler<VolumeEventArgs> VolumeChanged;
        public event EventHandler<InputEventArgs> InputChanged;
        public event EventHandler<QuadViewEventArgs> QuadViewChanged;
        public event EventHandler Connected;
        public event EventHandler Disconnected;

        public void Connect()
        {
            throw new NotImplementedException();
        }

        //pass events from TCP client up to consumer class
        private void TcpClient_DataReceived(object sender, DataReceivedArgs e)
        {
            /*
             * Parse the data sent by the display, check for changes
             * in properties that the IDisplay interfaces uses, and
             * raise events accordingly
             */

            //example response: "MULTI.VIEW:QUAD"
            string Response = e.DataString;

            if (Response.IndexOf("POWR")>=0)
            {
                string p = Response.Substring(Response.IndexOf("POWR"), 4);
                string powerStatus = Response.Substring(0, 1);
                switch (powerStatus)
                {
                    case "0":
                    {
                        // call event handler and set 
                    }
                    break;

                    case "1":
                    {
                            // call event handler and set 
                    }
                    break;
                } 
            }

            
        }

        private void TcpClient_Disconnected(object sender, EventArgs e)
        {
            Disconnected?.Invoke(this, e);
        }
        private void TcpClient_Connected(object sender, EventArgs e)
        {
            Connected?.Invoke(this, e);
        }

        public Sony(string ipAddress)
        {
            //this type of display does not support quad-view
            SupportsQuad = false;

            //initialize tcpClient
            //Sony displays use port 20060 for control
            //register tcpClient events
            tcpClient = new PtTcpClient(ipAddress, 20060);
            tcpClient.DataReceived += TcpClient_DataReceived;
            tcpClient.Connected += TcpClient_Connected;
            tcpClient.Disconnected += TcpClient_Disconnected;
            IpAddress = ipAddress;
        }

        private void SetPower(bool _state)
        {
            string powerCommand = _state ? "POWR1       " : "POWR0      ";
            tcpClient.SendString(powerCommand);
        }

        private void SetQuadview(bool _state)
        {
            if (SupportsQuad)
            {
                //string Command = state ? "Multi.View=Quad\r" : "Multi.View=Single\r";
                //tcpClient.SendString(Command);
            }
            else { throw new NotSupportedException("Display does not support quadview"); }
        }
    }
}
