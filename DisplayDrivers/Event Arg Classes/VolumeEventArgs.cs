﻿using System;

namespace DisplayDrivers
{
    public class VolumeEventArgs : EventArgs
    {
        public ushort CurrentLevel { get; internal set; }
    }
}
