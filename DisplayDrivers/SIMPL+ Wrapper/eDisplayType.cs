﻿
namespace DisplayDrivers
{
    public enum eDisplayType : ushort
    {
        PlanarUltraRes,
        Sony
        //more display types will go here as they are written
    }
}
