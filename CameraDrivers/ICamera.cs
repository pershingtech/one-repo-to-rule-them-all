﻿using System;

namespace CameraDrivers
{
    public interface ICamera
    {
        void Connect();
        bool ConnectState { get; }
        string UserName { get; set; }
        string Password { get; set; }
        event EventHandler Connected;
        event EventHandler Disconnected;

        bool Power { set; }

        PtzSpeed Speed { get; set; }

        void TiltUp(bool State);
        void TiltDown(bool State);
        void PanLeft(bool State);
        void PanRight(bool State);
        void ZoomIn(bool State);
        void ZoomOut(bool State);
        void Home();

        void SavePreset(ushort Number);
        void RecallPreset(ushort Number);
    }
}
