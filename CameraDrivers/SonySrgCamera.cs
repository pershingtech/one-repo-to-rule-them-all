﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Crestron.SimplSharp;

namespace CameraDrivers
{
    public class SonySrgCamera : ICamera
    {
        public SonySrgCamera()
        {
            iSpeed = 0;
        }
        public SonySrgCamera(string IpAddress)
        {
            iSpeed = 0;
            this.IpAddress = IpAddress;
        }
        public SonySrgCamera(string IpAddress, string UserName, string Password)
        {
            iSpeed = 0;
            this.IpAddress = IpAddress;
            this.UserName = UserName;
            this.Password = Password;

            httpClient = SetupClientWithDigestAuth();
        }

        public bool ConnectState { get; private set; }

        public bool Power
        {
            set
            {
                string state = value ? "on" : "standby";
                SendRequest($"{urlStart}/main.cgi?System={state}").GetAwaiter().GetResult();
            }
        }

        private PtzSpeed _ptzSpeed;
        public PtzSpeed Speed 
        {
            get { return _ptzSpeed; }
            set
            {
                _ptzSpeed = value;
                switch(_ptzSpeed)
                {
                    case PtzSpeed.Slow: { iSpeed = 8; break; }
                    case PtzSpeed.Medium: { iSpeed = 12; break; }
                    case PtzSpeed.Fast: { iSpeed = 16; break; }
                }
            }
        }
        private int iSpeed;

        public event EventHandler Connected;
        public event EventHandler Disconnected;

        public void Connect()
        {
            throw new NotImplementedException();
        }

        public async void Home()
        {
            await SendRequest($"{urlStart}/presetposition.cgi?HomePos=recall");
        }

        public async void PanLeft(bool State)
        {
            string state = State ? $"left%2C{iSpeed}" : "stop%2Cmotor";
            await SendRequest($"{urlStart}/ptzf.cgi?Move={state}");
        }
        public async void PanRight(bool State)
        {
            string state = State ? $"right%2C{iSpeed}" : "stop%2Cmotor";
            await SendRequest($"{urlStart}/ptzf.cgi?Move={state}");
        }
        public async void TiltDown(bool State)
        {
            string state = State ? $"down%2C{iSpeed}" : "stop%2Cmotor";
            await SendRequest($"{urlStart}/ptzf.cgi?Move={state}");
        }
        public async void TiltUp(bool State)
        {
            string state = State ? $"up%2C{iSpeed}" : "stop%2Cmotor";
            await SendRequest($"{urlStart}/ptzf.cgi?Move={state}");
        }
        public async void ZoomIn(bool State)
        {
            string state = State ? $"tele%2C{iSpeed}" : "stop%2Czoom";
            await SendRequest($"{urlStart}/ptzf.cgi?Move={state}");
        }
        public async void ZoomOut(bool State)
        {
            string state = State ? $"wide%2C{iSpeed}" : "stop%2Czoom";
            await SendRequest($"{urlStart}/ptzf.cgi?Move={state}");
        }

        public async void RecallPreset(ushort Number)
        {
            await SendRequest($"{urlStart}/presetposition.cgi?PresetCall={Number + 1}");
        }
        public async void SavePreset(ushort Number)
        {
            await SendRequest($"{urlStart}/presetposition.cgi?PresetSet={Number}%2Cpreset{Number}%2Con");
        }

        private string _ipAddress;
        public string IpAddress
        {
            get { return _ipAddress; }
            set
            {
                _ipAddress = value;
                urlStart = $"http://{_ipAddress}/command";
            }
        }

        public string UserName { get; set; }
        public string Password { get; set; }

        private string urlStart;

        private async Task SendRequest(string Url)
        {
            try
            {
                Uri requestUri;
                bool uriResult = Uri.TryCreate(Url, UriKind.RelativeOrAbsolute, out requestUri);
                if(uriResult)
                {
                    HttpRequestMessage requestMessage = new HttpRequestMessage();
                    requestMessage.Method = HttpMethod.Get;
                    requestMessage.RequestUri = requestUri;

                    var result = await httpClient.SendAsync(requestMessage);

                    ConnectState = result.IsSuccessStatusCode;
                    if(!result.IsSuccessStatusCode)
                    {
                        Disconnected?.Invoke(this, new EventArgs());
                        CrestronConsole.PrintLine($"{_ipAddress} error sending HTTP request: {result.StatusCode}");
                    }
                    else { Connected?.Invoke(this, new EventArgs()); }
                }
                else
                {
                    CrestronConsole.PrintLine($"{_ipAddress} error: Could not create HTTP URI");
                }
            }
            catch (Exception e)
            {
                CrestronConsole.PrintLine($"{_ipAddress} error: {e.Message}");
            }
        }

        private HttpClient httpClient;
        private HttpClient SetupClientWithDigestAuth()
        {
            var credCache = new CredentialCache();
            credCache.Add(new Uri($"http://{_ipAddress}"), "Digest", new NetworkCredential(UserName, Password));
            HttpClientHandler httpClientHandler = new HttpClientHandler
            {
                Credentials = credCache
            };

            return new HttpClient(httpClientHandler);
        }

        public void InitializeClient()
        {
            httpClient = SetupClientWithDigestAuth();
        }

        //SIMPL+ methods
        public async void PowerOnPlus() { this.Power = true; }
        public async void PowerOffPlus() { this.Power = false; }
        public async void PanLeftPlus(ushort State) { PanLeft((State > 0)); }
        public async void PanRightPlus(ushort State) { PanRight((State > 0)); }
        public async void TiltDownPlus(ushort State) { TiltDown((State > 0)); }
        public async void TiltUpPlus(ushort State) { TiltUp((State > 0)); }
        public async void ZoomInPlus(ushort State) { ZoomIn((State > 0)); }
        public async void ZoomOutPlus(ushort State) { ZoomOut((State > 0)); }
    }
}
