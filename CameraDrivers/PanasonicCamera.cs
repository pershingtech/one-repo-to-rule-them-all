﻿using System;
using System.Text;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Crestron.SimplSharp;

namespace CameraDrivers
{
    public class PanasonicCamera : ICamera
    {
        //constructors
        public PanasonicCamera(string IpAddress, string UserName, string Password)
        {
            this.IpAddress = IpAddress;
            this.UserName = UserName;
            this.Password = Password;
        }
        public PanasonicCamera(string IpAddress)
        {
            this.IpAddress = IpAddress;
        }

        //client-related objects
        private HttpClient httpClient = new HttpClient();
        private HttpRequestMessage httpRequest;
        
        private string _ipAddress;
        public string IpAddress
        {
            get { return _ipAddress; }
            set
            {
                _ipAddress = value;
                urlStart = String.Format("http://{0}/cgi-bin/aw_ptz?cmd=%23", this.IpAddress);
            }
             
        }

        public string UserName { get; set; }
        public string Password { get; set; }

        //private void SetCredentials()
        //{ 
        //    //add basic authorization header to the httpRequest that will be sent
        //    if (_userName.Length > 0 && _password.Length > 0)
        //    {
        //        string credentials = String.Format("{0}:{1}", _userName, _password);
        //        string encodedCredentials = Convert.ToBase64String(Encoding.ASCII.GetBytes(credentials));

        //        httpRequest.Headers.Authorization = new AuthenticationHeaderValue("Basic", encodedCredentials);
        //    }
        //}

        private HttpRequestMessage GetHttpRequest()
        {
            HttpRequestMessage request = new HttpRequestMessage();
            request.Method = HttpMethod.Get;
            if (UserName.Length > 0 && Password.Length > 0)
            {
                string credentials = String.Format("{0}:{1}", UserName, Password);
                string encodedCredentials = Convert.ToBase64String(Encoding.ASCII.GetBytes(credentials));

                request.Headers.Authorization = new AuthenticationHeaderValue("Basic", encodedCredentials);
            }
            return request;
        }

        private string urlStart;

        //connection logic
        public void Connect()
        {
            throw new NotImplementedException();
        }

        private bool _connectState;
        public bool ConnectState { get { return _connectState; } }
        public event EventHandler Connected;
        public event EventHandler Disconnected;

        //command sending method
        /// <summary>
        /// Asynchronously sends the command request to the camera.
        /// </summary>
        /// <param name="Command"></param>
        /// <returns>True if the request was successful, otherwise false</returns>
        private async Task<bool> SendCommand(string Command)
        {
            try
            {
                //format the uri string
                string sUri = String.Format("{0}{1}&res=1", urlStart, Command);

                //create URI object
                Uri requestUri;
                bool result = Uri.TryCreate(sUri, UriKind.RelativeOrAbsolute, out requestUri);

                //create httprequest
                HttpRequestMessage httpRequest = GetHttpRequest();

                //add the uri to the httpRequest object if successfully created
                if (result)
                    httpRequest.RequestUri = requestUri;
                else CrestronConsole.PrintLine("Error creating URI for Panasonic command");

                //send the request
                var sendResult = await httpClient.SendAsync(httpRequest);

                //if it is not successful, raise disconnected event and set state to false
                if (!sendResult.IsSuccessStatusCode)
                {
                    _connectState = false;
                    Disconnected?.Invoke(this, new EventArgs());
                    return false;
                }
                else if (!_connectState) //only raise the connected event if previously disconnected
                {
                    _connectState = true;
                    Connected?.Invoke(this, new EventArgs());
                }
                return true;
            }
            catch (Exception e){ CrestronConsole.PrintLine(e.Message); return false; }
        }

        //power
        public bool Power
        { 
            set
            {
                if (value)
                    SendCommand("O1");
                else SendCommand("O0");
            }
        }

        //speed setup
        private PtzSpeed _speed;
        private int LeftSpeed;
        private int RightSpeed;
        private int UpSpeed;
        private int DownSpeed;
        private int ZoomInSpeed;
        private int ZoomOutSpeed;

        private const int SlowOffset = 10;
        private const int MedOffset = 25;
        private const int FastOffset = 40;
        
        public PtzSpeed Speed
        {
            get { return _speed; }
            set
            {
                _speed = value;
                int SpeedOffset;
                switch (_speed)
                {
                    case PtzSpeed.Slow:     { SpeedOffset = SlowOffset; break; }
                    case PtzSpeed.Medium:   { SpeedOffset = MedOffset; break; }
                    case PtzSpeed.Fast:     { SpeedOffset = FastOffset; break; }
                    default:                { SpeedOffset = MedOffset; break; }
                }
                //left max: 01, min 49
                //right max: 99, min 51
                LeftSpeed = 50 - SpeedOffset;
                RightSpeed = 50 + SpeedOffset;

                //down max: 01, min 49
                //up max: 99, min 51
                DownSpeed = 50 - SpeedOffset;
                UpSpeed = 50 + SpeedOffset;

                //zoom out max: 01, min 49
                //zoom in max: 99, min 51
                ZoomOutSpeed = 50 - SpeedOffset;
                ZoomInSpeed = 50 + SpeedOffset;
            }
        }

        //manual PTZ
        public async void Home()
        {
            await SendCommand("APC80008000");
        }
        public async void PanLeft(bool State)
        {
            if (State)
                await SendCommand("P" + LeftSpeed.ToString("X2"));
            else await SendCommand("P50");
        }
        public async void PanRight(bool State)
        {
            if (State)
                await SendCommand("P" + RightSpeed.ToString("X2"));
            else await SendCommand("P50");
        }

        public async void TiltUp(bool State)
        {
            if (State)
                await SendCommand("T" + UpSpeed.ToString("X2"));
            else await SendCommand("T50");
        }
        public async void TiltDown(bool State)
        {
            if (State)
                await SendCommand("T" + DownSpeed.ToString("X2"));
            else await SendCommand("T50");
        }
        public async void ZoomIn(bool State)
        {
            if (State)
                await SendCommand("Z" + ZoomInSpeed.ToString("X2"));
            else await SendCommand("Z50");
        }
        public async void ZoomOut(bool State)
        {
            if (State)
                await SendCommand("Z" + ZoomOutSpeed.ToString("X2"));
            else await SendCommand("Z50");
        }
        
        //presets
        public async void RecallPreset(ushort Number)
        {
            if (Number <= 100)
                await SendCommand("R" + Number.ToString("X2"));
            else throw new NotSupportedException("Panasonic preset number out of range");
        }
        public async void SavePreset(ushort Number)
        {
            if (Number <= 100)
                await SendCommand("M" + Number.ToString("X2"));
            else throw new NotSupportedException("Panasonic preset number out of range");
        }

    }
}
