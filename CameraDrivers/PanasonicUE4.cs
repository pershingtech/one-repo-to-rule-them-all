﻿using System;
using System.Text;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Crestron.SimplSharp;

namespace CameraDrivers
{
    public class PanasonicUE4 : ICamera
    {
        //constructors
        public PanasonicUE4(string IpAddress, string UserName, string Password)
        {
            this.IpAddress = IpAddress;
            this.UserName = UserName;
            this.Password = Password;
        }
        public PanasonicUE4(string IpAddress)
        {
            this.IpAddress = IpAddress;
        }

        //client-related objects
        private HttpClient httpClient = new HttpClient();
        private HttpRequestMessage httpRequest;

        private string _ipAddress;
        public string IpAddress
        {
            get { return _ipAddress; }
            set
            {
                _ipAddress = value;
                urlStart = String.Format("http://{0}/storks/?cmd=", this.IpAddress);
            }

        }

        public string UserName { get; set; }
        public string Password { get; set; }

        //private void SetCredentials()
        //{ 
        //    //add basic authorization header to the httpRequest that will be sent
        //    if (_userName.Length > 0 && _password.Length > 0)
        //    {
        //        string credentials = String.Format("{0}:{1}", _userName, _password);
        //        string encodedCredentials = Convert.ToBase64String(Encoding.ASCII.GetBytes(credentials));

        //        httpRequest.Headers.Authorization = new AuthenticationHeaderValue("Basic", encodedCredentials);
        //    }
        //}

        private HttpRequestMessage GetHttpRequest()
        {
            HttpRequestMessage request = new HttpRequestMessage();
            request.Method = HttpMethod.Get;
            if (UserName.Length > 0 && Password.Length > 0)
            {
                string credentials = String.Format("{0}:{1}", UserName, Password);
                string encodedCredentials = Convert.ToBase64String(Encoding.ASCII.GetBytes(credentials));

                request.Headers.Authorization = new AuthenticationHeaderValue("Basic", encodedCredentials);
            }
            return request;
        }

        private string urlStart;

        //connection logic
        public void Connect()
        {
            throw new NotImplementedException();
        }

        private bool _connectState;
        public bool ConnectState { get { return _connectState; } }
        public event EventHandler Connected;
        public event EventHandler Disconnected;

        //command sending method
        /// <summary>
        /// Asynchronously sends the command request to the camera.
        /// </summary>
        /// <param name="Command"></param>
        /// <returns>True if the request was successful, otherwise false</returns>
        private async Task<bool> SendCommand(string Command)
        {
            try
            {
                //format the uri string
                string sUri = String.Format("{0}{1}&res=1", urlStart, Command);

                //create URI object
                Uri requestUri;
                bool result = Uri.TryCreate(sUri, UriKind.RelativeOrAbsolute, out requestUri);

                //create httprequest
                HttpRequestMessage httpRequest = GetHttpRequest();

                //add the uri to the httpRequest object if successfully created
                if (result)
                    httpRequest.RequestUri = requestUri;
                else CrestronConsole.PrintLine("Error creating URI for Panasonic command");

                //send the request
                var sendResult = await httpClient.SendAsync(httpRequest);

                //if it is not successful, raise disconnected event and set state to false
                if (!sendResult.IsSuccessStatusCode)
                {
                    _connectState = false;
                    Disconnected?.Invoke(this, new EventArgs());
                    return false;
                }
                else if (!_connectState) //only raise the connected event if previously disconnected
                {
                    _connectState = true;
                    Connected?.Invoke(this, new EventArgs());
                }
                return true;
            }
            catch (Exception e) { CrestronConsole.PrintLine(e.Message); return false; }
        }

        //power
        public bool Power
        {
            set
            {
                if (value)
                    SendCommand("O1");
                else SendCommand("O0");
            }
        }

        //manual PTZ
        public async void Home()
        {
            await SendCommand("zoom_reset");
        }
        public async void PanLeft(bool State)
        {
            if (State)
                await SendCommand("ptz_left_start:1");
            else await SendCommand("ptz_left_end");
        }
        public async void PanRight(bool State)
        {
            if (State)
                await SendCommand("ptz_right_start:1");
            else await SendCommand("ptz_right_end");
        }
        public async void TiltUp(bool State)
        {
            if (State)
                await SendCommand("ptz_up_start:1");
            else await SendCommand("ptz_up_end");
        }
        public async void TiltDown(bool State)
        {
            if (State)
                await SendCommand("ptz_down_start:1");
            else await SendCommand("ptz_down_end");
        }
        public async void ZoomIn(bool State)
        {
            if (State)
                await SendCommand("zoom_in_start");
            else await SendCommand("zoom_in_end");
        }
        public async void ZoomOut(bool State)
        {
            if (State)
                await SendCommand("zoom_out_start");
            else await SendCommand("zoom_out_end");
        }

        public async void RecallPreset(ushort Number)
        {
            if (Number <= 100)
                await SendCommand("load_preset:" + Number);
            else throw new NotSupportedException("Panasonic preset number out of range");
        }
        public async void SavePreset(ushort Number)
        {
            if (Number <= 100)
                await SendCommand("set_preset:" + Number);
            else throw new NotSupportedException("Panasonic preset number out of range");
        }

        //speed
        public PtzSpeed Speed { get; set; }
    }
}
