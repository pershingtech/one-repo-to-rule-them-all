﻿namespace CameraDrivers
{
    public enum PtzSpeed
    {
        Slow,
        Medium,
        Fast
    }
}
