﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NvxInterface
{
    public class DiscoveredEndpoint
    {
        public string Hostname;
        public string IpAddress;
        public string MacAddress;
    }
}
