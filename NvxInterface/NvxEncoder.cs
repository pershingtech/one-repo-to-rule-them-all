﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace NvxInterface
{
    public class NvxEncoder : NvxBase
    {
        //constructors
        public NvxEncoder(string Hostname) : base(Hostname)
        { 
            ClientInitialized += NvxEncoder_ClientInitialized;
            StreamUrl = string.Empty;
            AudioMcast = string.Empty;
        }
        public NvxEncoder(string Hostname, string UserName, string Password) : base(Hostname, UserName, Password)
        { 
            ClientInitialized += NvxEncoder_ClientInitialized;
            StreamUrl = string.Empty;
            AudioMcast = string.Empty;
        }
        
        //initialize properties when httpClient is setup
        private async void NvxEncoder_ClientInitialized(object sender, EventArgs e)
        {
            var parsedStreamLocation = NvxJson.StreamTxLocation.ParseResponse(DeviceJson);
            if (parsedStreamLocation.Length < 10)
                StreamUrl = String.Format($"rtsp://{IpAddress}:554/live.sdp");
            else StreamUrl = parsedStreamLocation;
            Console.WriteLine($"{IpAddress} stream URL: " + StreamUrl);

            AudioMcast = NvxJson.AudioTxAddress.ParseResponse(DeviceJson);
            Console.WriteLine($"{IpAddress} audio MCast: " + AudioMcast);
        }

        //read-only properties
        public string StreamUrl { get; private set; }
        public string AudioMcast { get; private set; }
        
        //controllable properties
        public bool StreamStarted { get; private set; }
        public async Task SetStreamStarted(bool State)
        {
            string messagePayload = NvxJson.StreamStarted(State);
            var result = await SendRequest(messagePayload, HttpMethod.Post);

            if(result.IsSuccessStatusCode)
                StreamStarted = State;
            else Console.WriteLine($"Error sending stream start to {Hostname} at {IpAddress}");
        }
    }
}