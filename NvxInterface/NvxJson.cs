﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using System.IO;
using System.Net.NetworkInformation;

namespace NvxInterface
{
    public static class NvxJson
    {
        static NvxJson()
        {
        }

        //read-only properties
        public static class StreamTxLocation
        {
            public static JObject JSON;
            public static string UrlPath;

            public static string ParseResponse(string JsonResponse)
            {
                JObject jTemp = JObject.Parse(JsonResponse);
                return jTemp["Device"]["StreamTransmit"]["Streams"][0]["StreamLocation"].ToString();
            }

            static StreamTxLocation()
            {
                JSON = JObject.Parse(jStreamTxLocation);
                UrlPath = GetUrlPath(JSON, "StreamLocation");
            }
        }
        public static class AudioTxAddress
        {
            public static JObject JSON;
            public static string UrlPath;

            public static string ParseResponse(string JsonResponse)
            {
                JObject jTemp = JObject.Parse(JsonResponse);
                return jTemp["Device"]["NaxAudio"]["NaxTx"]["NaxTxStreams"]["Stream1"]["NetworkAddressStatus"].ToString();
            }

            static AudioTxAddress()
            {
                JSON = JObject.Parse(jAudioTxStream);
                UrlPath = GetUrlPath(JSON, "NetworkAddressStatus");
            }
        }
        public static class Hostname
        {
            public static JObject JSON;
            public static string UrlPath;

            public static string ParseResponse(string JsonResponse)
            {
                JObject jTemp = JObject.Parse(JsonResponse);
                return jTemp["Device"]["Ethernet"]["HostName"].ToString();
            }

            static Hostname()
            {
                JSON = JObject.Parse(jHostname);
                UrlPath = GetUrlPath(JSON, "HostName");
            }
        }
        public static class MacAddress
        {
            public static JObject JSON;
            public static string UrlPath;

            public static string ParseResponse(string JsonResponse)
            {
                JObject jTemp = JObject.Parse(JsonResponse);
                return jTemp["Device"]["DeviceInfo"]["MacAddress"].ToString();
            }

            static MacAddress()
            {
                JSON = JObject.Parse(jHostname);
                UrlPath = GetUrlPath(JSON, "MacAddress");
            }
        }
        public static class UsbLocalId
        {
            public static JObject JSON;
            public static string UrlPath;
            
            public static string ParseResponse(string JsonResponse)
            {
                JObject jTemp = JObject.Parse(JsonResponse);
                return jTemp["Device"]["Usb"]["UsbPorts"][0]["UsbPairing"]["Layer2"]["LocalDeviceId"].ToString();
            }

            static UsbLocalId()
            {
                JSON = JObject.Parse(jUsbLocalDeviceId);
                //UrlPath = GetUrlPath(JSON, "LocalDeviceId");
                UrlPath = "Device/Usb";
            }
        }
        private static string GetUrlPath(JObject jObject, string TokenName)
        {
            string jPath = jObject.FindTokens(TokenName)[0].Path;
            string urlPath = jPath.Replace('.', '/');
            return urlPath;
        }

        //write properties
        public static string StreamRxLocation(string SourceUrl)
        {
            JObject jTemp = JObject.Parse(jStreamRxLocation);
            jTemp["Device"]["StreamReceive"]["Streams"][0]["StreamLocation"] = SourceUrl;
            return jTemp.ToString();

        }
        public static string AudioSource(string Source)
        {
            JObject jTemp = JObject.Parse(jAudioSource);
            jTemp["Device"]["DeviceSpecific"]["AudioSource"] = Source;
            return jTemp.ToString();
        }
        public static string AudioStreamReceiveMode(string Mode)
        {
            JObject jTemp = JObject.Parse(jAudioReceiveMode);
            jTemp["Device"]["NaxAudio"]["SecondaryAudioRx"]["AudioMode"] = Mode;
            return jTemp.ToString();
        }
        public static string SecondaryAudioStreamAddress(string McastAddress)
        {
            JObject jTemp = JObject.Parse(jAudioRxStream);
            jTemp["Device"]["NaxAudio"]["NaxRx"]["NaxRxStreams"]["Stream1"]["NetworkAddressRequested"] = McastAddress;
            return jTemp.ToString();
        }
        public static string AudioFollowsVideo(bool State)
        {
            JObject jTemp = JObject.Parse(jAudioBreakaway);
            jTemp["Device"]["NaxAudio"]["SecondaryAudioRx"]["AudioMode"] = State ? eAudioReceiveStreamMode.Automatic : eAudioReceiveStreamMode.Breakaway;
            jTemp["Device"]["DeviceSpecific"]["AudioSource"] = State ? eAudioSources.AudioFollowsVideo : eAudioSources.SecondaryStream;
            return jTemp.ToString();
        }

        public static string UsbRemoteDeviceId(string DeviceId)
        {
            JObject jTemp = JObject.Parse(jUsbRemoveDeviceId);
            jTemp["Device"]["Usb"]["UsbPorts"][0]["UsbPairing"]["Layer2"]["RemoteDevices"]["Id1"] = DeviceId;
            return jTemp.ToString();
        }
        public static string UsbAutoPair(bool State)
        {
            JObject jTemp = JObject.Parse(jUsbAutopair);
            jTemp["Device"]["Usb"]["UsbPorts"][0]["IsAutoUsbPairingEnabled"] = State;
            return jTemp.ToString();
        }
        public static string UsbManualPair()
        {
            JObject jTemp = JObject.Parse(jUsbManualPair);
            return jTemp.ToString();
        }
        public static string UsbManualUnpair()
        {
            JObject jTemp = JObject.Parse(jUsbManualUnpair);
            return jTemp.ToString();
        }
        public static string UsbMode(string Mode)
        {
            JObject jTemp = JObject.Parse(jUsbMode);
            jTemp["Device"]["Usb"]["UsbPorts"][0]["Mode"] = Mode;
            return jTemp.ToString();
        }

        public static string StreamStarted(bool State)
        {
            JObject jTemp;
            jTemp = State ? JObject.Parse(jStreamStart) : JObject.Parse(jStreamStop);
            return jTemp.ToString();
        }
        public static string OutputBlanked(bool State)
        {
            JObject jtemp = JObject.Parse(jBlankOutput);
            jtemp["Device"]["AudioVideoInputOutput"]["Outputs"][0]["Ports"][0]["Hdmi"]["IsBlankingDisabled"] = State;
            return jtemp.ToString();
        }
        public static string OutputDisabled(bool State)
        {
            JObject jtemp = JObject.Parse(jDisableOutput);
            jtemp["Device"]["AudioVideoInputOutput"]["Outputs"][0]["Ports"][0]["Hdmi"]["IsOutputDisabled"] = State;
            return jtemp.ToString();
        }

        #region Json Strings
        //typical routing
        private const string jStreamTxLocation = 
            @"{
                'Device': {
                'StreamTransmit': {
                    'Streams': [
                        {
                            'StreamLocation': ''
                        }
                    ]
                }
                }
            }";
        private const string jStreamRxLocation =
            @"{
                'Device': {
                'StreamReceive': {
                    'Streams': [
                        {
                            'StreamLocation': 'rtsp://192.168.86.200:554/live.sdp'
                        }
                    ]
                }
                }
            }";
        private const string jHostname =
            @"{
              'Device': {
                'Ethernet': {
                  'HostName': ''
                }
              }
            }";
        private const string jMacAddress =
            @"{
              'Device': {
                'DeviceInfo': {
                  'MacAddress': ''
                }
              }
            }";

        //audio breakaway
        private const string jAudioReceiveMode =
            @"{
              'Device': {
                'NaxAudio': {
                  'SecondaryAudioRx': {
                    'AudioMode': ''
                  }
                }
              }
            }";
        private const string jAudioSource =
            @"{
              'Device': {
                'DeviceSpecific': {
                  'AudioSource': 'AudioFollowsVideo'
                }
              }
            }";
        private const string jAudioTxStream =
            @"{
              'Device': {
                'NaxAudio': {
                  'NaxTx': {
                    'NaxTxStreams': {
                      'Stream1': {
                        'NetworkAddressStatus': ''
                      }
                    }
                  }
                }
              }
            }";
        private const string jAudioRxStream =
            @"{
              'Device': {
                'NaxAudio': {
                  'NaxRx': {
                    'NaxRxStreams': {
                      'Stream1': {
                        'NetworkAddressRequested': ''
                      }
                    }
                  }
                }
              }
            }";
        private const string jAudioBreakaway =
            @"{
              'Device': {
                'NaxAudio': {
                  'SecondaryAudioRx': {
                    'AudioMode': ''
                  }
                },
                'DeviceSpecific': {
                  'AudioSource': 'AudioFollowsVideo'
                }
              }
            }
            ";

        //usb
        private const string jUsbLocalDeviceId =
            @"{
              'Device': {
                'Usb': {
                  'UsbPorts': [
                    {
                      'UsbPairing': {
                        'Layer2': {
                          'LocalDeviceId': ''
                        }
                      }
                    }
                  ]
                }
              }
            }";
        private const string jUsbRemoveDeviceId =
            @"{
              'Device': {
                'Usb': {
                  'UsbPorts': [
                    {
                      'UsbPairing': {
                        'Layer2': {
                          'RemoteDevices': {
                            'Id1': ''
                          }
                        }
                      }
                    }
                  ]
                }
              }
            }";
        private const string jUsbAutopair =
            @"{
              'Device': {
                'Usb': {
                  'UsbPorts': [
                    {
                      'IsAutoUsbPairingEnabled': false
                    }
                  ]
                }
              }
            }";
        private const string jUsbManualPair =
            @"{
              'Device': {
                'Usb': {
                  'UsbPorts': [
                    {
                      'Pair': true
                    }
                  ]
                }
              }
            }";
        private const string jUsbManualUnpair =
            @"{
              'Device': {
                'Usb': {
                  'UsbPorts': [
                    {
                      'Unpair': true
                    }
                  ]
                }
              }
            }";
        private const string jUsbMode =
            @"{
              'Device': {
                'Usb': {
                  'UsbPorts': [
                    {
                      'Mode': 'Local'
                    }
                  ]
                }
              }
            }";

        //misc other control points
        private const string jAutoInputSelection =
            @"{
              'Device': {
                'DeviceSpecific': {
                  'AutoInputRoutingEnabled': false
                }
              }
            }";
        private const string jBlankOutput =
            @"{
              'Device': {
                'AudioVideoInputOutput': {
                  'Outputs': [
                    {
                      'Ports': [
                        {
                          'Hdmi': {
                            'IsBlankingDisabled': false
                          }
            }
                      ]
                    }
                  ]
                }
              }
            }";
        private const string jDisableOutput =
            @"{
              'Device': {
                'AudioVideoInputOutput': {
                  'Outputs': [
                    {
                      'Ports': [
                        {
                          'Hdmi': {
                            'IsOutputDisabled': false
                          }
            }
                      ]
                    }
                  ]
                }
              }
            }";
        private const string jVideoWallMode =
            @"{
              'Device': {
                'DeviceSpecific': {
                  'VideoWallMode': ''
                }
              }
            }";

        private const string jStreamStart =
            @"{
            'Device':{
                    'StreamTransmit':{
                        'Streams':[
                        {
                            'Start':true
                        }
                        ]
                    }
                }
            }
            ";
        private const string jStreamStop =
            @"{
            'Device':{
                    'StreamTransmit':{
                        'Streams':[
                        {
                            'Stop':true
                        }
                        ]
                    }
                }
            }
            ";
        #endregion
    }
    static class JsonExtensions
    {
        public static List<JToken> FindTokens(this JToken containerToken, string name)
        {
            List<JToken> matches = new List<JToken>();
            FindTokens(containerToken, name, matches);
            return matches;
        }

        private static void FindTokens(JToken containerToken, string name, List<JToken> matches)
        {
            if (containerToken.Type == JTokenType.Object)
            {
                foreach (JProperty child in containerToken.Children<JProperty>())
                {
                    if (child.Name == name)
                    {
                        matches.Add(child.Value);
                    }
                    FindTokens(child.Value, name, matches);
                }
            }
            else if (containerToken.Type == JTokenType.Array)
            {
                foreach (JToken child in containerToken.Children())
                {
                    FindTokens(child, name, matches);
                }
            }
        }
    }
}