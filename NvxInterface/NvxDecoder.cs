﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace NvxInterface
{
    public class NvxDecoder : NvxBase
    {
        //constructors
        public NvxDecoder(string Hostname) : base(Hostname) { }
        public NvxDecoder(string Hostname, string UserName, string Password) : base(Hostname, UserName, Password) { }

        /*******routing properties & methods************/
        public string StreamUrl { get; private set; }
        public async Task SetStreamUrl(string Url)
        {
            if (Url != null)
            {
                string messagePayload = NvxJson.StreamRxLocation(Url);
                var result = await SendRequest(messagePayload, HttpMethod.Post);
                if (result.IsSuccessStatusCode)
                    StreamUrl = Url;
                else Console.WriteLine($"Error sending Stream URL POST to {Hostname} at {IpAddress}");
            }
        }

        public string AudioMcast { get; private set; }
        public async Task SetAudioMcast(string Address)
        {
            if (!AudioFollowsVideo)
            {
                string messagePayload = NvxJson.SecondaryAudioStreamAddress(Address);
                var result = await SendRequest(messagePayload, HttpMethod.Post);
                if (result.IsSuccessStatusCode)
                    AudioMcast = Address;
                else Console.WriteLine($"Error sending audio MCast address to {Hostname} at {IpAddress}");
            }
            else Console.WriteLine($"Must set {Hostname}'s audio follows video to false before setting Audio MCast address");
        }

        public bool AudioFollowsVideo { get; private set; }
        public async Task SetAudioFollowsVideo(bool State)
        {
            string messagePayload = NvxJson.AudioFollowsVideo(State);
            var result = await SendRequest(messagePayload, HttpMethod.Post);
            if (result.IsSuccessStatusCode)
                AudioFollowsVideo = State;
            else Console.WriteLine($"Error sending audio follows video to {Hostname} at {IpAddress}");
        }
    }
}
