﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Timers;
using System.Threading.Tasks;
using System.Threading;
using Crestron.SimplSharp;

namespace NvxInterface
{
    public class NvxBase
    {
        //constructors
        public NvxBase(string Hostname)
        {
            this.Hostname = Hostname;
            UserName = "admin";
            Password = "admin";
            KeepAliveTimer.Elapsed += KeepAliveTimer_Elapsed;

            NvxManager.AllEndpoints.Add(this);
        }

        public NvxBase(string Hostname, string UserName, string Password)
        {
            this.Hostname = Hostname;
            this.UserName = UserName;
            this.Password = Password;
            KeepAliveTimer.Elapsed += KeepAliveTimer_Elapsed;

            NvxManager.AllEndpoints.Add(this);
        }

        //network properties
        public string Hostname { get; private set; }
        public string IpAddress { get; private set; }

        public async Task SetIpAddress(string Address)
        {
            try
            {
                IpAddress = Address;
                CrestronConsole.PrintLine($"Setting up HTTP client for {Hostname} at {IpAddress}");
                httpClient = await GetHttpClient(Address);
                //CrestronConsole.PrintLine("Client initialized, getting device JSON");
                var getDevice = await SendRequest(String.Empty, HttpMethod.Get);
                DeviceJson = await getDevice.Content.ReadAsStringAsync();
                await InitializeCommonProperties();
                ClientInitialized?.Invoke(this, new EventArgs());
            }
            catch (Exception e) { CrestronConsole.PrintLine($"Error setting up HTTP client for hostname {Hostname} at {Address}: {e.Message}"); }
        }

        public string MacAddress { get; private set; }

        public string UserName { get; private set; }
        public string Password { get; private set; }

        //http client & methods
        public HttpClient httpClient;
        public async Task<HttpClient> GetHttpClient(string IpAddress)
        {
            KeepAliveTimer.Stop();

            //setup client handler to ignore the self-signed certificate error
            HttpClientHandler httpClientHandler = new HttpClientHandler
            {
                ServerCertificateCustomValidationCallback = (message, cert, chain, errors) => { return true; }
            };
            httpClientHandler.AllowAutoRedirect = false;

            //create temporary client with the above handler
            HttpClient tempClient = new HttpClient(httpClientHandler);

            tempClient.Timeout = new TimeSpan(0, 0, 10);

            //remove expect continue (code 100) header
            tempClient.DefaultRequestHeaders.ExpectContinue = false;
            tempClient.DefaultRequestHeaders.Add("Connection", "Keep-Alive");

            //get login page. We don't do anything with the resulting page
            //but it gives tempClient a cookie to use for loggin in
            var loginUrl = $"https://{IpAddress}/userlogin.html";
            var loginPage = await tempClient.GetAsync(loginUrl);

            if(!loginPage.IsSuccessStatusCode)
                CrestronConsole.PrintLine($"{IpAddress} Get login error: " + loginPage.StatusCode);

            //create POST request to send login credentials
            //endpoint wants credentials as POST request body
            HttpRequestMessage postLogin = new HttpRequestMessage(HttpMethod.Post, loginUrl);
            postLogin.Headers.ExpectContinue = false;
            var sLoginContent = $"login={UserName}&passwd={Password}";
            StringContent loginContent = new StringContent(sLoginContent);
            postLogin.Content = loginContent;

            //CrestronConsole.PrintLine($"{IpAddress} Posting login credentials");
            var loginResult = await tempClient.SendAsync(postLogin);

            if(!loginResult.IsSuccessStatusCode)
                CrestronConsole.PrintLine($"{IpAddress} Login post error: " + loginResult.StatusCode);

            KeepAliveTimer.Start();
            return tempClient;
        }
        public static async Task<HttpClient> GetNvxClient(string IpAddress, string UserName, string Password)
        {
            try
            {
                //KeepAliveTimer.Stop();

                //setup client handler to ignore the self-signed certificate error
                HttpClientHandler httpClientHandler = new HttpClientHandler
                {
                    ServerCertificateCustomValidationCallback = (message, cert, chain, errors) => { return true; }
                };
                httpClientHandler.AllowAutoRedirect = false;

                //create temporary client with the above handler
                HttpClient tempClient = new HttpClient(httpClientHandler);

                tempClient.Timeout = new TimeSpan(0, 0, 10);

                //remove expect continue (code 100) header
                tempClient.DefaultRequestHeaders.ExpectContinue = false;
                tempClient.DefaultRequestHeaders.Add("Connection", "Keep-Alive");

                //get login page. We don't do anything with the resulting page
                //but it gives tempClient a cookie to use for loggin in
                var loginUrl = $"https://{IpAddress}/userlogin.html";
                var loginPage = await tempClient.GetAsync(loginUrl);

                if (!loginPage.IsSuccessStatusCode)
                    Console.WriteLine($"{IpAddress} Get login error: " + loginPage.StatusCode);

                //create POST request to send login credentials
                //endpoint wants credentials as POST request body
                HttpRequestMessage postLogin = new HttpRequestMessage(HttpMethod.Post, loginUrl);
                postLogin.Headers.ExpectContinue = false;
                //postLogin.Headers.Add("Content-Type", "application/x-www-form-urlencoded");
                var sLoginContent = $"login={UserName}&passwd={Password}";
                StringContent loginContent = new StringContent(sLoginContent, Encoding.UTF8, "application/x-www-form-urlencoded");
                postLogin.Content = loginContent;

                //CrestronConsole.PrintLine($"{IpAddress} Posting login credentials");
                var loginResult = await tempClient.SendAsync(postLogin);

                if (!loginResult.IsSuccessStatusCode)
                    Console.WriteLine($"{IpAddress} Login post error: " + loginResult.StatusCode);

                //KeepAliveTimer.Start();
                return tempClient;
            }
            catch (Exception e)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(e.Message);
            }

            return null;
        }
        private System.Timers.Timer KeepAliveTimer = new System.Timers.Timer(120000); 
        private async void KeepAliveTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            try
            {
                //CrestronConsole.PrintLine($"{IpAddress} Sending keep alive");
                string uri = $"https://{IpAddress}/Device/DeviceInfo/Name";
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, uri);
                var result = await httpClient.SendAsync(request);

                if (!result.IsSuccessStatusCode)
                    CrestronConsole.PrintLine($"Error sending keep alive to NVX {Hostname} at {IpAddress}");
            }
            catch (Exception ex) { CrestronConsole.PrintLine(ex.Message); }
        }

        protected async Task<HttpResponseMessage> SendRequest(string Content, HttpMethod Method)
        {
            TimeSpan retryInterval = new TimeSpan(25);
            int maxAttempts = 3;
            var exceptions = new List<Exception>();
            for(int attempt = 0; attempt < maxAttempts; attempt++)
            {
                try
                {
                    if (attempt > 0)
                        Thread.Sleep(retryInterval);

                    string uri = $"https://{IpAddress}/Device";
                    HttpRequestMessage httpRequestMessage = new HttpRequestMessage(Method, uri);

                    if (Method == HttpMethod.Post)
                    {
                        StringContent stringContent = new StringContent(Content, Encoding.UTF8, "application/json");
                        httpRequestMessage.Content = stringContent;
                    }

                    var result = await httpClient.SendAsync(httpRequestMessage);
                    return result;
                }
                catch (Exception e)
                {
                    exceptions.Add(e);
                }
            }

            foreach (Exception e in exceptions)
                CrestronConsole.PrintLine(e.Message);

            return null;
        }

        //items used when httpClient is initialize
        private async Task InitializeCommonProperties()
        {
            UsbLocalDeviceId = NvxJson.UsbLocalId.ParseResponse(DeviceJson);
            //CrestronConsole.PrintLine($"{IpAddress} USB Local ID: " + UsbLocalDeviceId);
        }
        protected event EventHandler ClientInitialized;
        protected string DeviceJson { get; private set; }

        //common properties
        public eAnalogAudioMode AnalogAudioMode { get; private set; }
        public async Task SetAnalogAudioMode(eAnalogAudioMode Mode)
        {
            AnalogAudioMode = Mode;
            //TODO: Send analog audio mode post request
        }

        public string UsbLocalDeviceId { get; private set; }

        public string UsbRemoteDeviceId { get; private set; }
        public async Task SetUsbRemoteDeviceId(string RemoteDeviceId)
        {
            string messagePayload = NvxJson.UsbRemoteDeviceId(RemoteDeviceId);
            var result = await SendRequest(messagePayload, HttpMethod.Post);

            if(result.IsSuccessStatusCode)
                UsbRemoteDeviceId = RemoteDeviceId;
            else CrestronConsole.PrintLine("Error sending USB route to hostname {0} at {1}", Hostname, IpAddress);
        }

        public bool UsbAutoPairEnabled { get; private set; }
        public async Task SetUsbUsbAutoPairEnabled(bool State)
        {
            string messagePayload = NvxJson.UsbAutoPair(UsbAutoPairEnabled);
            var result = await SendRequest(messagePayload, HttpMethod.Post);

            if(result.IsSuccessStatusCode)
                UsbAutoPairEnabled = State;
            else CrestronConsole.PrintLine("Error setting USB autopair to hostname {0} at {1}", Hostname, IpAddress);
        }

        public bool OutputEnabled { get; private set; }
        public async Task SetOutputEnabled(bool State)
        {
            string messagePayload = NvxJson.OutputDisabled(!State);
            var result = await SendRequest(messagePayload, HttpMethod.Post);

            if (result.IsSuccessStatusCode)
                OutputEnabled = State;
            else CrestronConsole.PrintLine("Error setting HDMI output enabled to hostame {0} at {1}", Hostname, IpAddress);
        }
    }
}