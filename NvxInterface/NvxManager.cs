﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Crestron.SimplSharp;

namespace NvxInterface
{
    public static class NvxManager
    {
        static NvxManager()
        {
            EndpointDiscovery.DiscoveryComplete += EndpointDiscovery_DiscoveryComplete;
        }

        private static async void EndpointDiscovery_DiscoveryComplete(object sender, EventArgs e)
        {
            await MatchEndpoints();
        }

        public static List<NvxBase> AllEndpoints = new List<NvxBase>();
        public static async Task MatchEndpoints()
        {
            foreach(NvxBase nvx in AllEndpoints)
            {
                //var match = EndpointDiscovery.DiscoveredEndpoints.FirstOrDefault(x => x.Key.ToUpper() == nvx.Hostname.ToUpper());
                //var match = EndpointDiscovery.DiscoveredEndpoints[nvx.Hostname.ToUpper()];
                if (EndpointDiscovery.DiscoveredEndpoints.ContainsKey(nvx.Hostname.ToUpper()))
                {
                    //await nvx.SetIpAddress(EndpointDiscovery.DiscoveredEndpoints[nvx.Hostname.ToUpper()]);
                    Thread myThread = new Thread(() => nvx.SetIpAddress(EndpointDiscovery.DiscoveredEndpoints[nvx.Hostname.ToUpper()]));
                    myThread.Start();
                }
                else CrestronConsole.PrintLine($"No match for NVX hostname {nvx.Hostname} found in device discovery");
            }
        }
    }
}
