﻿namespace NvxInterface
{
    public enum eNvxDeviceMode
    {
        Encoder,
        Decoder
    }
    public enum eClassification : ushort
    {
        Stateless = 0,
        Unclassified = 1,
        Secret = 2,
        ThirdNetwork = 3,
        TopSecret = 4
    }
    public enum eAnalogAudioMode
    {
        Insert,
        Extract
    }
    public enum eSourceType : ushort
    {
        Blank,
        Pc,
        VtcPrimary,
        VtcSecondary,
        Camera,
        Aux,
        Microphone
    }
    public enum eDestinationType : ushort
    {
        Display,
        Audio,
        VtcCamera,
        VtcContent
    }
    public static class eAudioSources
    {
        public const string AudioFollowsVideo = "AudioFollowsVideo";
        public const string Hdmi1 = "Input1";
        public const string Hdmi2 = "Input2";
        public const string Analog = "AnalogAudio";
        public const string PrimaryStream = "PrimaryStreamAudio";
        public const string SecondaryStream = "SecondaryStreamAudio";
    }
    public static class eVideoSources
    {
        public const string None = "None";
        public const string Hdmi1 = "Input1";
        public const string Hdmi2 = "Input2";
        public const string Stream = "Stream";
    }
    public static class eAudioReceiveStreamMode
    {
        public const string Automatic = "Automatic";
        public const string Breakaway = "Manual";
    }
    public static class eOutputResolutions
    {
        public const string Auto = "Auto";
        public const string SevenTwenty60 = "1280x720@60";
        public const string SevenTwenty50 = "1280x720@50";
        public const string NineteenTwenty60 = "1920x1080@60";
        public const string NineteenTwenty50 = "1920x1080@50";
        public const string FourK60 = "3840x2160@60";
        public const string FourK50 = "3840x2160@50";
        public const string FourK30 = "3840x2160@30";
        public const string Fourk24 = "3840x2160@24";
    }
    public static class eUsbModes
    {
        public const string Local = "Local";
        public const string Remote = "Remote";
    }
}
