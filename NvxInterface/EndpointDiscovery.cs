﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Net;
using System.Timers;
using Crestron.SimplSharp;


namespace NvxInterface
{
    public static class EndpointDiscovery
    {
        static EndpointDiscovery()
        {
            ListenTimer.Elapsed += ListenTimer_Elapsed;
        }

        private const int listenPort = 41794;
        private static UdpClient listener = new UdpClient(listenPort);
        private static System.Timers.Timer ListenTimer = new System.Timers.Timer(5000)
        {
            AutoReset = false
        };
        //private static bool timerRunning;
        
        public static Dictionary<string, string> DiscoveredEndpoints = new Dictionary<string, string>();
        //public static List<DiscoveredEndpoint> DiscoveredEndpoints = new List<DiscoveredEndpoint>();
        //public static void DiscoverEndpoints()
        //{
        //    DiscoveredEndpoints.Clear();

        //    byte[] dataBytes = { 0x14, 0x00, 0x00, 0x00, 0x01, 0x04, 0x00, 0x03,
        //         0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        //         0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        //         0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        //         0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        //         0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        //         0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        //         0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        //         0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        //         0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        //         0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        //         0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        //         0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        //         0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        //         0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        //         0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        //         0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        //         0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        //         0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        //         0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        //         0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        //         0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        //         0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        //         0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        //         0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        //         0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        //         0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        //         0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        //         0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        //         0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        //         0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        //         0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        //         0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        //         0x00, 0x00 };

        //    //get my hostname & IP Address
        //    var hostName = System.Net.Dns.GetHostName();
        //    var localHost = System.Net.Dns.GetHostEntry(hostName);
        //    var localIPs = localHost.AddressList;

        //    //send discovery broadcast
        //    listener.Client.ReceiveTimeout = 5000;
        //    listener.Send(dataBytes, dataBytes.Length, "255.255.255.255", listenPort);

        //    //ListenTimer.Start();
        //    //timerRunning = true;

        //    try
        //    {
        //        while (ListenTimer.Enabled)
        //        {
        //            var receiveResult = await listener.ReceiveAsync();

        //            Console.WriteLine("Received from " + receiveResult.RemoteEndPoint.Address);
        //            byte[] dataReceived = receiveResult.Buffer;

        //            int addressPosition = Array.IndexOf(localIPs, receiveResult.RemoteEndPoint.Address);
        //            if (addressPosition < 0)
        //                ParseBytes(dataReceived, receiveResult.RemoteEndPoint.Address);
        //        }
        //        Console.WriteLine("Listener timed out");
        //    }
        //    catch (Exception e) { Console.WriteLine(e.Message); }

        //    foreach (DiscoveredEndpoint ep in DiscoveredEndpoints)
        //        CrestronConsole.PrintLine($"NVX hostname {ep.Hostname} found at {ep.IpAddress}");

        //    DiscoveryComplete?.Invoke(null, new EventArgs());
        //}
        //private static void ParseBytes(byte[] bytes, System.Net.IPAddress senderIp)
        //{
        //    //Console.WriteLine(Encoding.Default.GetString(bytes));
        //    try
        //    {
        //        //parse data for model number of device
        //        //model number starts at byte 266 in response packet
        //        int firstByte = 266;

        //        //model number ends with " "
        //        byte byteToFind = Encoding.ASCII.GetBytes(" ")[0];

        //        int lastByte = Array.IndexOf(bytes, byteToFind, firstByte);
        //        byte[] parsedBytes = new byte[lastByte - firstByte];
        //        Array.Copy(bytes, firstByte, parsedBytes, 0, lastByte - firstByte);

        //        string modelNumber = Encoding.Default.GetString(parsedBytes);

        //        if (modelNumber.ToUpper().IndexOf("NVX") >= 0)
        //        {
        //            //parse hostname
        //            firstByte = 10;
        //            byteToFind = Convert.ToByte(0);
        //            lastByte = Array.IndexOf(bytes, byteToFind, firstByte);

        //            parsedBytes = new byte[lastByte - firstByte];
        //            Array.Copy(bytes, firstByte, parsedBytes, 0, lastByte - firstByte);

        //            string nvxHostName = Encoding.Default.GetString(parsedBytes);

        //            //parse mac address
        //            byte toFind = 0x5D;
        //            int macStartIndex = Array.IndexOf(bytes, toFind) + 5;

        //            //Console.WriteLine(BitConverter.ToString(bytes));
        //            Console.WriteLine("macStart: " + macStartIndex);
        //            byte[] macBytes = new byte[12];
        //            Array.Copy(bytes, macStartIndex, macBytes, 0, 12);
        //            string nvxMac = Encoding.Default.GetString(macBytes);

        //            Console.WriteLine("Parsed MAC: " + nvxMac);

        //            DiscoveredEndpoints.Add(new DiscoveredEndpoint()
        //            {
        //                Hostname = nvxHostName,
        //                IpAddress = senderIp.ToString(),
        //                MacAddress = nvxMac
        //            });
        //        }
        //    }
        //    catch (Exception e) { Console.WriteLine(e.Message); }
        //}

        private static Dictionary<System.Net.IPEndPoint, byte[]> DiscoveryResponses = new Dictionary<System.Net.IPEndPoint, byte[]>();
        private static System.Net.IPHostEntry localHost;
        private static void StartListener()
        {
            DiscoveredEndpoints.Clear();
            try
            {
                System.Net.IPEndPoint groupEP = new System.Net.IPEndPoint(System.Net.IPAddress.Any, listenPort);
                listener.Client.ReceiveTimeout = 5000;

                //data bytes captured by WireShark from Device Discovery broadcast
                //starting at byte[10], sender hostname is inserted in the array
                byte[] dataBytes = { 0x14, 0x00, 0x00, 0x00, 0x01, 0x04, 0x00, 0x03,
                 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                 0x00, 0x00 };

                CrestronConsole.PrintLine("Running Auto-Discovery...");

                //get my hostname & IP Address
                var hostName = System.Net.Dns.GetHostName();
                localHost = System.Net.Dns.GetHostEntry(hostName);
                //var bHostName = Encoding.ASCII.GetBytes(hostName);

                //insert my hostname in data bytes to send in broadcast
                //for (int i = 0; i < bHostName.Length; i++)
                //{
                //    dataBytes[i + 10] = bHostName[i];
                //}

                //send discovery broadcast
                DiscoveredEndpoints.Clear();
                DiscoveryResponses.Clear();
                listener.Send(dataBytes, dataBytes.Length, "255.255.255.255", listenPort);

                while (ListenTimer.Enabled)
                {
                    //Console.WriteLine("Listener enabled, receiving data...");
                    byte[] bytes = listener.Receive(ref groupEP);

                    //Console.WriteLine($"{groupEP.Address}");
                    if (!DiscoveryResponses.ContainsKey(groupEP))
                        DiscoveryResponses.Add(groupEP, bytes);
                    //else Console.WriteLine($"groupEP {groupEP.Address} already in collection");
                    //Console.WriteLine("Crestron device at: {0}", groupEP.Address);
                    //if (!localHost.AddressList.Contains(groupEP.Address))
                    //{
                    //    Thread thread = new Thread(() => ParseDiscoveryResponse(bytes, groupEP));
                    //    thread.Start();
                    //}
                }

                //Console.WriteLine("Timer expired, parsing results");
                //foreach (KeyValuePair<System.Net.IPEndPoint, byte[]> kvp in DiscoveryResponses)
                //    ParseDiscoveryResponse(kvp.Value, kvp.Key);

                //DiscoveryComplete?.Invoke(null, new EventArgs());
            }
            catch (Exception e)
            {
                //Console.WriteLine(e.Message);
                //Console.WriteLine("Discovery Error Source: " + e.Source);
                //Console.WriteLine("Discovery trace: " + e.StackTrace);

            }
            finally
            {
                //Console.WriteLine("Made it to finally");
                //listener.Close();
                CrestronConsole.PrintLine("Discovery timer expired, parsing results");
                foreach (KeyValuePair<System.Net.IPEndPoint, byte[]> kvp in DiscoveryResponses)
                    ParseDiscoveryResponse(kvp.Value, kvp.Key);

                //order dictionary by IP address
                DiscoveredEndpoints = DiscoveredEndpoints.OrderBy(x => x.Value).ToDictionary(x => x.Key, x => x.Value);

                var lookup = DiscoveredEndpoints.ToLookup(x => x.Value, x => x.Key).Where(x => x.Count() > 1);
                foreach(var item in lookup)
                {
                    var keys = item.Aggregate("", (s, v) => s + ", " + v);
                    var message = "NVX hostnames have IP address" + item.Key + ":" + keys;
                    CrestronConsole.PrintLine(message);
                }

                DiscoveryComplete?.Invoke(null, new EventArgs());
            }
        }
        private static void ParseDiscoveryResponse(byte[] bytes, System.Net.IPEndPoint groupEP)
        {
            try
            {
                if (!localHost.AddressList.Contains(groupEP.Address))
                {
                    //CrestronConsole.PrintLine($"Crestron device discovered at: {groupEP.Address}");
                    //parse data for model
                    //model number starts at byte 266 in response packet
                    int firstByte = 266;

                    //model number ends with " "
                    byte byteToFind = Encoding.ASCII.GetBytes(" ")[0];

                    int lastByte = Array.IndexOf(bytes, byteToFind, firstByte);
                    byte[] parsedBytes = new byte[lastByte - firstByte];
                    Array.Copy(bytes, firstByte, parsedBytes, 0, lastByte - firstByte);

                    string modelNumber = Encoding.Default.GetString(parsedBytes);

                    if (modelNumber.ToUpper().IndexOf("NVX") >= 0)
                    {
                        //parse hostname
                        firstByte = 10;
                        byteToFind = Convert.ToByte(0);
                        lastByte = Array.IndexOf(bytes, byteToFind, firstByte);

                        parsedBytes = new byte[lastByte - firstByte];
                        Array.Copy(bytes, firstByte, parsedBytes, 0, lastByte - firstByte);

                        string nvxHostName = Encoding.Default.GetString(parsedBytes);
                        CrestronConsole.PrintLine("NVX Endpoint {0} Found At: {1}", nvxHostName, groupEP.Address);
                        DiscoveredEndpoints.Add(nvxHostName, groupEP.Address.ToString());
                    }
                }
            }
            catch (Exception e)
            {
                CrestronConsole.PrintLine(e.Message);
                CrestronConsole.PrintLine(e.StackTrace);
            }
        }
        private static void ListenTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            ListenTimer.Stop();
            //foreach (KeyValuePair<string, string> kvp in DiscoveredEndpoints)
            //    CrestronConsole.PrintLine($"NVX hostname {kvp.Key} found at {kvp.Value}");
            //DiscoveryComplete?.Invoke(null, new EventArgs());
        }

        public static void StartDiscovery()
        {
            ListenTimer.Start();
            StartListener();
        }

        public static event EventHandler DiscoveryComplete;
    }
}